package cs838;

import java.io.*;
import java.util.*;

public class FileParser {
    // parse the header of a file
    private static int parseHeader(BufferedReader reader, ArrayList<HashMap<String, Float>> feature_to_val, HashMap<String, Float> label_to_val){
        int num_of_features = -1;
        int num_of_examples = -1;
        try{
            String content;
            while((content = reader.readLine()) != null){
                content = content.trim();
                if(content.length() == 0 || content.startsWith("//"))
                    continue;
                if(num_of_features == -1){
                    num_of_features = Integer.parseInt(content);
                    continue;
                }
                if(content.indexOf("-") != -1){
                    // map catagorical feature into numerical value
                    String[] lists = content.split("-")[1].trim().split(" ");
                    HashMap<String, Float> mapping = new HashMap<String, Float>();
                    for(int i=0; i<lists.length; i++){
                        String s = lists[i].trim();
                        if(s.length() == 0)
                            continue;
                        mapping.put(s, new Float(mapping.size()));
                    }
                    feature_to_val.add(mapping);
                }
                else{
                    try{
                        int val = Integer.parseInt(content);
                        num_of_examples = val;
                        break;
                    }catch(NumberFormatException e){
                        label_to_val.put(content, new Float(label_to_val.size()));
                    }
                }
            }
            assert num_of_features == feature_to_val.size();
        }catch(Exception e){
            e.printStackTrace();
        }
        return num_of_examples;
    }

    public static void loadFromFile(String filename, ArrayList<ArrayList<Float>> feature, ArrayList<Float> label, ArrayList<String> name,
                                    ArrayList<HashMap<String, Float>> feature_to_val, HashMap<String, Float> label_to_val){
        try{
            int num_of_examples = 0;
            BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
            // parse the header of data file, and map feature into numbers
            if(label_to_val.size() == 0 && feature_to_val.size() == 0)
                num_of_examples = parseHeader(reader, feature_to_val, label_to_val);
            else{
                ArrayList<HashMap<String, Float>> feature_map = new ArrayList<HashMap<String, Float>>();
                HashMap<String, Float> label_map = new HashMap<String, Float>();
                num_of_examples = parseHeader(reader, feature_map, label_map);
                // some simple assert to check all the files have the same header
                assert feature_to_val.size() == feature_map.size();
                assert label_to_val.size() == label_map.size();
            }
            String content;
            while((content = reader.readLine()) != null){
                // read data sample
                content = content.trim();
                if(content.length() == 0 || content.startsWith("//"))
                    continue;
                String[] lists = content.split(" |\t");
                ArrayList<String> non_empty = new ArrayList<String>();
                for(int i=0; i<lists.length; i++){
                    String s = lists[i].trim();
                    if(s.length() == 0)
                        continue;
                    non_empty.add(s);
                }
                name.add(non_empty.get(0));
                label.add(label_to_val.get(non_empty.get(1)));
                ArrayList<Float> sample = new ArrayList<Float>();
                for(int i=2; i<non_empty.size(); i++)
                    sample.add(feature_to_val.get(i-2).get(non_empty.get(i)));
                assert sample.size() == feature_to_val.size();
                feature.add(sample);
            }
            reader.close();
            assert feature.size() == num_of_examples;
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
