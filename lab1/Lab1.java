import java.io.*;
import java.util.*;

class FileParser {
    // parse the header of a file
    private static int parseHeader(BufferedReader reader, ArrayList<HashMap<String, Float>> feature_to_val, HashMap<String, Float> label_to_val){
        int num_of_features = -1;
        int num_of_examples = -1;
        try{
            String content;
            while((content = reader.readLine()) != null){
                content = content.trim();
                if(content.length() == 0 || content.startsWith("//"))
                    continue;
                if(num_of_features == -1){
                    num_of_features = Integer.parseInt(content);
                    continue;
                }
                if(content.indexOf("-") != -1){
                    // map catagorical feature into numerical value
                    String[] lists = content.split("-")[1].trim().split(" ");
                    HashMap<String, Float> mapping = new HashMap<String, Float>();
                    for(int i=0; i<lists.length; i++){
                        String s = lists[i].trim();
                        if(s.length() == 0)
                            continue;
                        mapping.put(s, new Float(mapping.size()));
                    }
                    feature_to_val.add(mapping);
                }
                else{
                    try{
                        int val = Integer.parseInt(content);
                        num_of_examples = val;
                        break;
                    }catch(NumberFormatException e){
                        label_to_val.put(content, new Float(label_to_val.size()));
                    }
                }
            }
            assert num_of_features == feature_to_val.size();
        }catch(Exception e){
            e.printStackTrace();
        }
        return num_of_examples;
    }

    public static void loadFromFile(String filename, ArrayList<ArrayList<Float>> feature, ArrayList<Float> label, ArrayList<String> name,
                                    ArrayList<HashMap<String, Float>> feature_to_val, HashMap<String, Float> label_to_val){
        try{
            int num_of_examples = 0;
            BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
            // parse the header of data file, and map feature into numbers
            if(label_to_val.size() == 0 && feature_to_val.size() == 0)
                num_of_examples = parseHeader(reader, feature_to_val, label_to_val);
            else{
                ArrayList<HashMap<String, Float>> feature_map = new ArrayList<HashMap<String, Float>>();
                HashMap<String, Float> label_map = new HashMap<String, Float>();
                num_of_examples = parseHeader(reader, feature_map, label_map);
                // some simple assert to check all the files have the same header
                assert feature_to_val.size() == feature_map.size();
                assert label_to_val.size() == label_map.size();
            }
            String content;
            while((content = reader.readLine()) != null){
                // read data sample
                content = content.trim();
                if(content.length() == 0 || content.startsWith("//"))
                    continue;
                String[] lists = content.split(" |\t");
                ArrayList<String> non_empty = new ArrayList<String>();
                for(int i=0; i<lists.length; i++){
                    String s = lists[i].trim();
                    if(s.length() == 0)
                        continue;
                    non_empty.add(s);
                }
                name.add(non_empty.get(0));
                label.add(label_to_val.get(non_empty.get(1)));
                ArrayList<Float> sample = new ArrayList<Float>();
                for(int i=2; i<non_empty.size(); i++)
                    sample.add(feature_to_val.get(i-2).get(non_empty.get(i)));
                assert sample.size() == feature_to_val.size();
                feature.add(sample);
            }
            reader.close();
            assert feature.size() == num_of_examples;
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

public class Lab1 {

    class Node {
        public ArrayList<Float> weight;
        public Float w0;
        public Float output;

        // Lab1 node, random initialize data
        public Node(int dim){
            Random rnd = new Random();
            weight = new ArrayList<Float>();
            for(int i=0; i<dim; i++)
                weight.add(new Float(rnd.nextFloat() / 50.0 - 0.01));
            w0 = new Float(rnd.nextFloat() / 50.0 - 0.01);
        }
    }

    private ArrayList<ArrayList<Float>> feature_train;
    private ArrayList<ArrayList<Float>> feature_tune;
    private ArrayList<ArrayList<Float>> feature_test;

    private ArrayList<Float> label_train;
    private ArrayList<Float> label_tune;
    private ArrayList<Float> label_test;

    private ArrayList<String> name_train;
    private ArrayList<String> name_tune;
    private ArrayList<String> name_test;

    private ArrayList<HashMap<String, Float>> feature_to_val;
    private HashMap<String, Float> label_to_val;

    private Float learning_rate;

    private Node perceptron;

    public Lab1(){
        feature_train = new ArrayList<ArrayList<Float>>();
        feature_tune = new ArrayList<ArrayList<Float>>();
        feature_test = new ArrayList<ArrayList<Float>>();

        label_train = new ArrayList<Float>();
        label_tune = new ArrayList<Float>();
        label_test = new ArrayList<Float>();

        name_train = new ArrayList<String>();
        name_tune = new ArrayList<String>();
        name_test = new ArrayList<String>();

        feature_to_val = new ArrayList<HashMap<String, Float>>();
        label_to_val = new HashMap<String, Float>();

        learning_rate = new Float(0.01);
    }

    // forward and backward propagation
    private void forward(ArrayList<Float> instance){
        Float sum = 0.0f;
        for(int i=0; i<instance.size(); i++)
            sum += instance.get(i) * perceptron.weight.get(i);
        sum += perceptron.w0;
        perceptron.output = new Float(1.0 / (1.0 + Math.exp(-1.0 * sum)));
    }

    private void backward(ArrayList<Float> instance, Float correct){
        for(int i=0; i<instance.size(); i++){
            Float update = instance.get(i) * (correct - perceptron.output) * learning_rate;
            perceptron.weight.set(i, update + perceptron.weight.get(i));
        }
        perceptron.w0 += (correct - perceptron.output) * learning_rate;
    }

    public void train(){
        Float best_accuracy = new Float(0.0);
        int count = 0;
        while(true){
            // forward and backward propagation
            for(int i=0; i<feature_train.size(); i++){
                forward(feature_train.get(i));
                backward(feature_train.get(i), label_train.get(i));
            }
            ArrayList<Float> tune_output = evaluate(feature_tune);
            Float accuracy = checkAccuracy(label_tune, tune_output);
            // early stopping, if accuracy does not increase in three iterations, stop the algorithm
            if(accuracy > best_accuracy){
                best_accuracy = accuracy;
                count = 0;
            }
            else{
                count += 1;
                if(count == 3)
                    break;
            }
        }
    }

    // check the accuracy of a list of output with the correct answer
    private Float checkAccuracy(ArrayList<Float> correct, ArrayList<Float> output){
        int counter = 0;
        for(int i=0; i<correct.size(); i++){
            if(output.get(i) < 0.5 && correct.get(i) == 0.0)
                counter++;
            else if(output.get(i) >= 0.5 && correct.get(i) == 1.0)
                counter++;
        }
        return new Float(counter) / new Float(correct.size());
    }

    // evaluate the accuracy on a dataset
    private ArrayList<Float> evaluate(ArrayList<ArrayList<Float>> feature){
        ArrayList<Float> output = new ArrayList<Float>();
        for(int i=0; i<feature.size(); i++){
            forward(feature.get(i));
            output.add(perceptron.output);
        }
        return output;
    }

    private void printResult(ArrayList<Float> result){
        HashMap<Float, String> val_to_label = new HashMap<Float, String>();
        for(Map.Entry<String, Float> entry : label_to_val.entrySet()){
            val_to_label.put(entry.getValue(), entry.getKey());
        }
        for(int i=0; i<result.size(); i++){
            if(result.get(i) < 0.5)
                System.out.println(val_to_label.get(new Float(0.0)));
            else
                System.out.println(val_to_label.get(new Float(1.0)));
        }
    }

    public void test(){
        ArrayList<Float> test_output = evaluate(feature_test);
        printResult(test_output);
        Float accuracy = checkAccuracy(label_test, test_output);
        System.out.println(accuracy);
    }

    public void init(String train_file, String tune_file, String test_file){
        FileParser.loadFromFile(train_file, feature_train, label_train, name_train, feature_to_val, label_to_val);
        FileParser.loadFromFile(tune_file, feature_tune, label_tune, name_tune, feature_to_val, label_to_val);
        FileParser.loadFromFile(test_file, feature_test, label_test, name_test, feature_to_val, label_to_val);

        perceptron = new Node(feature_to_val.size());
    }

    public static void main(String[] args) {
        Lab1 nn = new Lab1();
        nn.init(args[0], args[1], args[2]);
        nn.train();
        nn.test();
    }
}
