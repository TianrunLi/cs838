package cs838;

import java.util.*;

class Vector {
    public float[] data;

    public float get(int i){
        return data[i];
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i=0; i<data.length-1; i++){
            sb.append(data[i]);
            sb.append(", ");
        }
        sb.append(data[data.length-1]);
        sb.append("]");
        return sb.toString();
    }

    public void set(float[] source){
        data = source;
    }

    // Different constructors
    public Vector(float[] source){
        data = source;
    }

    public Vector(int sz){
        data = new float[sz];
    }

    public Vector(){
        data = null;
    }
}

public class VectorMath{
    public static Random rnd = new Random();

    public static void add(Vector operand1, Vector operand2, Vector destination){
        float[] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<dst.length; i++)
            dst[i] = op1[i] + op2[i];
    }

    public static void minus(Vector operand1, Vector operand2, Vector destination){
        float[] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<dst.length; i++)
            dst[i] = op1[i] - op2[i];
    }

    public static void mult(Vector operand, float val, Vector destination){
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++)
            dst[i] = op[i] * val;
    }

    public static void vmult(Vector operand1, Vector operand2, Vector destination){
        float[] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<op1.length; i++)
            dst[i] = op1[i] * op2[i];
    }

    public static void div(Vector operand, float val, Vector destination){
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++)
            dst[i] = op[i] / val;
    }

    public static void cprod(Vector operand1, Vector operand2, Matrix destination){
        // cross product of two vector
        float[] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[][] dst = destination.data;
        for(int i=0; i<op1.length; i++)
            for(int j=0; j<op2.length; j++)
                dst[i][j] = op1[i] * op2[j];
    }

    public static float iprod(Vector operand1, Vector operand2){
        // inner product of two vector
        float[] op1 = operand1.data;
        float[] op2 = operand2.data;
        float dst = 0.0f;
        for(int i=0; i<op1.length; i++)
            dst += op1[i] * op2[i];
        return dst;
    }

    public static void relu(Vector operand, Vector destination){
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++){
            if(op[i] < 0.0f)
                dst[i] = 0.0f;
            else
                dst[i] = op[i];
        }
    }

    public static void drelu(Vector operand, Vector destination){
        // The derivitive of ReLU function
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++){
            if(op[i] == 0.0f)
                dst[i] = 0.0f;
            else
                dst[i] = 1.0f;
        }
    }

    public static void sigmoid(Vector operand, Vector destination){
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++)
            dst[i] = 1.0f / (1.0f + (float)Math.exp(-1 * op[i]));
    }

    public static void dsigmoid(Vector operand, Vector destination){
        // The derivitive of sigmoid function
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++)
            dst[i] = op[i] * (1.0f - op[i]);
    }

    public static void oneHot(Vector operand, int which){
        float[] op = operand.data;
        for(int i=0; i<op.length; i++)
            op[i] = 0.0f;
        op[which] = 1.0f;
    }

    public static int maxOverZero(Vector operand){
        float[] op = operand.data;
        float max = 0.0f;
        int which = 0;
        for(int i=0; i<op.length; i++){
            if(op[i] > max){
                max = op[i];
                which = i;
            }
        }
        return which;
    }

    public static void rand(Vector operand, float limit){
        // randomize vector in range [-limit, limit]
        float[] op = operand.data;
        float divide = 1.0f / limit / 2.0f;
        for(int i=0; i<op.length; i++)
            op[i] = rnd.nextFloat() / divide - limit;
    }

    public static void range(Vector operand, float start, float delta){
        // initialize vector to be an arithmetic sequence
        float[] op = operand.data;
        for(int i=0; i<op.length; i++)
            op[i] = start + i * delta;
    }

    public static void range(Vector operand){
        range(operand, 0.0f, 1.0f);
    }

    public static void zero(Vector operand){
        float[] op = operand.data;
        for(int i=0; i<op.length; i++)
            op[i] = 0.0f;
    }

    public static void mask(Vector operand, float drop_rate){
        // Set 'drop_rate'% of the data to be 1.0f, others 0.0f
        // Means 'drop_rate'% of the neuron is active
        float[] op = operand.data;
        for(int i=0; i<op.length; i++){
            float val = rnd.nextFloat();
            if(val < drop_rate)
                op[i] = 1.0f;
            else
                op[i] = 0.0f;
        }
    }

    public static void copy(Vector operand, Vector destination){
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++)
            dst[i] = op[i];
    }

    public static int size(Vector operand){
        // Size of a vector is the number of elements in this vector
        return operand.data.length;
    }

    public static Vector empty_like(Vector operand){
        float[] op = operand.data;
        return new Vector(op.length);
    }

    public static void main(String[] args){
        Vector v1 = new Vector(3);
        VectorMath.rand(v1, 0.1f);
        System.out.println(v1);

        v1 = new Vector(3);
        VectorMath.range(v1);
        v1.data[0] = 1.0f;

        Vector v2 = new Vector(3);
        VectorMath.range(v2);

        VectorMath.add(v1, v2, v1);
        VectorMath.add(v1, v1, v1);
        VectorMath.minus(v2, v1, v2);
        System.out.println(v1);
        System.out.println(v2);

        VectorMath.mult(v2, 3.0f, v2);
        v2.data[0] = 10.0f;
        System.out.println(v2);

        VectorMath.div(v2, 2.0f, v1);
        System.out.println(v1);

        System.out.println(VectorMath.iprod(v1, v2));

        System.out.println("");
        Matrix m = new Matrix(3, 3);
        VectorMath.cprod(v1, v2, m);
        System.out.println(m);
    }
}
