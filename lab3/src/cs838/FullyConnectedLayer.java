package cs838;

public class FullyConnectedLayer extends Layer{
    // Different kinds of activation function
    public static enum ActivationType {Relu, Sigmoid};

    // Other parameters for FullyConnectedLayer
    private static final float learning_rate = 0.0001f;
    private static final float momentum_factor = 0.9f;
    // drop_out_rate means keep a neuron active with this probability
    // See http://cs231n.github.io/neural-networks-2/ for details
    private static final float drop_out_rate = 0.5f;
    private static boolean during_dropout = true;

    // Output_prev points to the output of previous layer, use this in forward
    // Delta_next and weight_next points to the delta and weight of next layer, use this in backward
    public Vector output_prev;
    public Vector delta_next;
    public Matrix weight_next;

    // Parameters for this layer
    public Integer hidden_count;
    public boolean has_bias;

    // Fully Connected Layer takes vector as output, may have a bias parameter
    public Vector output;
    public Matrix weight;

    // Parameters for backpropagation
    public Vector dropout;
    public Vector delta;
    public Matrix delta_weight;
    public Matrix momentum;

    // Some temp vector or matrix to store intermediate value
    public Vector temp_vector;
    public Matrix temp_matrix;

    public ActivationType activation;

    public FullyConnectedLayer(int hiddenCount, boolean hasBias, ActivationType actType){
        super(Layer.LayerType.FullyConnected);
        has_bias = hasBias;
        hidden_count = hiddenCount;
        if(has_bias)
            hidden_count += 1;
        activation = actType;
        during_dropout = true;
    }

    public static void enableDropout(){
        during_dropout = true;
    }

    public static void disableDropout(){
        during_dropout = false;
    }

    public void createMatrix(int inputDim){
        output = new Vector(hidden_count);
        weight = new Matrix(hidden_count, inputDim);

        dropout = new Vector(hidden_count);
        delta = new Vector(hidden_count);
        delta_weight = new Matrix(hidden_count, inputDim);
        momentum = new Matrix(hidden_count, inputDim);

        temp_vector = new Vector(hidden_count);
        temp_matrix = new Matrix(hidden_count, inputDim);
    }

    public void layerAsMiddle(Layer prev){
        // If this is not the first layer, the previous layer can be a FullyConnectedLayer or NormalizeLayer
        if(prev.layer_type == Layer.LayerType.FullyConnected){
            FullyConnectedLayer prevLayer = (FullyConnectedLayer)prev;
            output_prev = prevLayer.output;
        }
        else if(prev.layer_type == Layer.LayerType.Normalize){
            NormalizeLayer prevLayer = (NormalizeLayer)prev;
            output_prev = prevLayer.output;
        }
        else{
            throw new RuntimeException("The previous layer of a FullyConnectedLayer can only be FullyConnectedLayer or NormalizeLayer");
        }
        int lengthInput = output_prev.data.length;
        createMatrix(lengthInput);
    }

    public void layerAsBegin(int channel, int lengthInput){
        if(channel != 1){
            throw new RuntimeException("Channel parameter for FullyConnectedLayer can only be 1");
        }
        output_prev = new Vector(lengthInput);
        createMatrix(lengthInput);
    }

    public void copy(Layer destination){
        if(destination.layer_type != Layer.LayerType.FullyConnected){
            throw new RuntimeException("Can only copy between two FullyConnected layers");
        }
        FullyConnectedLayer dst = (FullyConnectedLayer)destination;
        MatrixMath.copy(weight, dst.weight);
    }

    public void init(Layer next){
        if(next == null){
            // If this is the last layer
            weight_next = null;
            delta_next = null;
        }
        else{
            // If this is not the last layer, the next layer can only be a FullyConnectedLayer
            if(next.layer_type == Layer.LayerType.FullyConnected){
                FullyConnectedLayer nextLayer = (FullyConnectedLayer)next;
                weight_next = nextLayer.weight;
                delta_next = nextLayer.delta;
            }
            else{
                throw new RuntimeException("The next layer of a FullyConnectedLayer can only be a FullyConnectedLayer");
            }
        }

        // Randomize the parameter, using the random setting from Jude's code, but the new version of init works worse than previous...
        int fanin = output_prev.data.length;
        int fanout = hidden_count;
        /*
        float rand_limit = (float)Math.max(10 * Float.MIN_VALUE,
                                            activation == ActivationType.relu 
                                                ? 2.0 / (float)Math.sqrt(fanin + fanout)
                                                : 4.0 * (float)Math.sqrt(6.0 / (fanin + fanout)));
                                                */
        float rand_limit = Math.max(Float.MIN_VALUE, 4.0f / (float)Math.sqrt(6.0f * (fanin + fanout)));
        MatrixMath.zero(momentum);
        MatrixMath.rand(weight, rand_limit);
    }

    public void forward(){
        MatrixMath.lprod(weight, output_prev, output);
        if(activation == ActivationType.Relu)
            VectorMath.relu(output, output);
        else if(activation == ActivationType.Sigmoid)
            VectorMath.sigmoid(output, output);
                // Code with 2 extra indent is for dropout
                // Random set some output nodes to be 0, as if they are removed from the network
                // Generate a new mask at each forward phase, and use the same mask in the backward phase
                if(during_dropout == true && weight_next != null){
                    VectorMath.mask(dropout, drop_out_rate);
                    VectorMath.vmult(output, dropout, output);
                    // Inverted dropout, scale the output if you use dropout
                    VectorMath.div(output, drop_out_rate, output);
                }

        // If this layer has a bias unit, set the corresponding output to 1.0f
        // Since the value calculated by matrix multiply is meaningless
        if(has_bias){
            int which = output.data.length - 1;
            output.data[which] = 1.0f;
        }
    }

    public void backward(){
        if(delta_next != null){
            MatrixMath.rprod(delta_next, weight_next, delta);
            if(activation == ActivationType.Relu)
                VectorMath.drelu(output, temp_vector);
            else if(activation == ActivationType.Sigmoid)
                VectorMath.dsigmoid(output, temp_vector);
            VectorMath.vmult(temp_vector, delta, delta);
                    // The code with 2 extra indent is for dropout
                    // Set the delta of nodes to be 0, use the same mask as in the forward phase
                    // as if they are removed from the network
                    if(during_dropout){
                        VectorMath.vmult(delta, dropout, delta);
                        VectorMath.div(delta, drop_out_rate, delta);
                    }
            // If this layer has a bias unit, set the corresponding delta to 0.0f
            // Otherwise this delta may interfere the delta calculation of previous layers
            if(has_bias){
                int which = delta.data.length - 1;
                delta.data[which] = 0.0f;
            }
        }

        VectorMath.cprod(delta, output_prev, delta_weight);
        MatrixMath.mult(delta_weight, learning_rate, delta_weight);
            // The code with 1 extra indent is for momentum
            MatrixMath.mult(momentum, momentum_factor, temp_matrix);
            MatrixMath.add(delta_weight, temp_matrix, delta_weight);
        MatrixMath.add(weight, delta_weight, weight);
            MatrixMath.copy(delta_weight, momentum);
    }

    public void calcError(Vector correct){
        // Currently use sigmoid + cross entropy function to calculate error
        if(delta_next != null || activation != ActivationType.Sigmoid){
            throw new RuntimeException("Calculate error only at the last level, using sigmoid function");
        }
        VectorMath.minus(correct, output, delta);
    }
}
