package cs838;

public class ConvolutionLayer extends Layer{

    private static final float learning_rate = 0.0001f;
    private static final float momentum_factor = 0.9f;
    private static final float drop_out_rate = 0.8f;
    private static boolean during_dropout = true;

    public int convolution_stride;
    public int kernel_size;
    public int number_kernel;

    public Tensor output_prev;
    // Currently just grab the pointer to the next layer
    public Layer next;

    public Tensor output;
    public Tensor delta;
    public Tensor dropout;
    public Tensor[] kernel;
    public Tensor[] kernel_delta;
    public Tensor[] momentum_kernel;
    public Vector bias;
    public Vector bias_delta;
    public Vector momentum_bias;

    // Temporary tensor with the size of delta or single kernel
    public Tensor temp_tensor_delta;
    public Tensor temp_tensor_kernel;
    public Vector temp_vector;

    public ConvolutionLayer(int stride, int kernelSize, int numKernel){
        super(Layer.LayerType.Convolution);
        convolution_stride = stride;
        kernel_size = kernelSize;
        number_kernel = numKernel;
    }

    public static void enableDropout(){
        during_dropout = true;
    }

    public static void disableDropout(){
        during_dropout = false;
    }

    public void createTensors(int outputLength, int channel){
        output = new Tensor(number_kernel, outputLength, outputLength);
        delta = new Tensor(number_kernel, outputLength, outputLength);
        dropout = new Tensor(number_kernel, outputLength, outputLength);
        bias = new Vector(number_kernel);
        bias_delta = new Vector(number_kernel);
        momentum_bias = new Vector(number_kernel);
        kernel = new Tensor[number_kernel];
        kernel_delta = new Tensor[number_kernel];
        momentum_kernel = new Tensor[number_kernel];
        for(int i=0; i<number_kernel; i++){
            kernel[i] = new Tensor(channel, kernel_size, kernel_size);
            kernel_delta[i] = new Tensor(channel, kernel_size, kernel_size);
            momentum_kernel[i] = new Tensor(channel, kernel_size, kernel_size);
        }
        temp_tensor_delta = new Tensor(number_kernel, outputLength, outputLength);
        temp_tensor_kernel = new Tensor(channel, kernel_size, kernel_size);
        temp_vector = new Vector(number_kernel);
    }

    public void layerAsMiddle(Layer prev){
        if(prev.layer_type == Layer.LayerType.Convolution){
            ConvolutionLayer prevLayer = (ConvolutionLayer)prev;
            output_prev = prevLayer.output;
        }
        else if(prev.layer_type == Layer.LayerType.Polling){
            PollingLayer prevLayer = (PollingLayer)prev;
            output_prev = prevLayer.output;
        }
        else{
            throw new RuntimeException("The previous layer of Convolution can only be Convolution or Polling");
        }
        int channel = output_prev.data.length;
        int inputLength = output_prev.data[0].data.length;
        int outputLength = MatrixMath.calcConvSize(inputLength, kernel_size, convolution_stride);
        createTensors(outputLength, channel);
    }

    public void layerAsBegin(int channel, int lengthInput){
        // Use this when ConvolutionLayer is the first layer in the network
        output_prev = new Tensor(channel, lengthInput, lengthInput);
        int outputLength = MatrixMath.calcConvSize(lengthInput, kernel_size, convolution_stride);
        createTensors(outputLength, channel);
    }

    public void copy(Layer destination){
        if(destination.layer_type != Layer.LayerType.Convolution){
            throw new RuntimeException("Can only copy between two Convolution layers");
        }
        // Kernel, bias and related parameters should be copied to destination
        ConvolutionLayer dst = (ConvolutionLayer)destination;
        dst.convolution_stride = convolution_stride;
        dst.kernel_size = kernel_size;
        dst.number_kernel = number_kernel;
        for(int i=0; i<kernel.length; i++)
            TensorMath.copy(kernel[i], dst.kernel[i]);
        VectorMath.copy(bias, dst.bias);
    }

    public void init(Layer n){
        // Do some check and save the next layer
        if(n.layer_type == Layer.LayerType.FullyConnected){
            throw new RuntimeException("Can not connect Polling directly with FullyConnected");
        }
        next = n;

        // Randomize the parameter
        // TODO: How to set an appropriate random weight at first?
        int channel = output_prev.data.length;
        int size = output_prev.data[0].data.length;
        int inputSize = channel * size * size;
        float rand_limit_tensor = 1.0f / (float)Math.sqrt(inputSize);
        for(int i=0; i<number_kernel; i++){
            TensorMath.rand(kernel[i], rand_limit_tensor);
        }
        // TODO: How to randomize this bias weight?
        float rand_limit_vector = 0.1f;
        VectorMath.rand(bias, rand_limit_vector);
        for(int i=0; i<number_kernel; i++)
            TensorMath.zero(momentum_kernel[i]);
        VectorMath.zero(momentum_bias);
    }
    
    public void forward(){
        // Convolution, add bias, and relu
        TensorMath.conv(output_prev, kernel, convolution_stride, output);
        TensorMath.convBias(output, bias);
        TensorMath.relu(output, output);
                // Code with 2 extra indent is for dropout
                if(during_dropout){
                    TensorMath.mask(dropout, drop_out_rate);
                    TensorMath.vmult(output, dropout, output);
                    TensorMath.div(output, drop_out_rate, output);
                }
    }

    public void backward(){
        // First, calculate the delta on this level
        if(next.layer_type == Layer.LayerType.Normalize){
            // If the next layer is normalize, the weight between this layer and next layer is always 1
            // So, just grab the delta from next layer and resize it
            NormalizeLayer nextLayer = (NormalizeLayer)next;
            TensorMath.resize(nextLayer.delta, delta);
        }
        else if(next.layer_type == Layer.LayerType.Polling){
            PollingLayer nextLayer = (PollingLayer)next;
            int next_stride = nextLayer.polling_stride;
            Tensor next_delta = nextLayer.delta;
            TensorMath.dpoll(output, next_delta, next_stride, delta);
        }
        else if(next.layer_type == Layer.LayerType.Convolution){
            ConvolutionLayer nextLayer = (ConvolutionLayer)next;
            Tensor next_delta = nextLayer.delta;
            Tensor[] next_kernel = nextLayer.kernel;
            Vector next_bias = nextLayer.bias;
            int next_convolution_stride = nextLayer.convolution_stride;

            TensorMath.zero(delta);
            for(int i=0; i<next_kernel.length; i++){
                TensorMath.dconvDeltaAdd(next_delta.get(i), next_kernel[i], next_convolution_stride, delta);
            }
            // Convolution layer has a relu activation function, so delta need to multiply the drelu
            TensorMath.drelu(output, temp_tensor_delta);
            TensorMath.vmult(delta, temp_tensor_delta, delta);
        }
                // Code with 2 extra indent is for dropout
                if(during_dropout){
                    TensorMath.vmult(delta, dropout, delta);
                    TensorMath.div(delta, drop_out_rate, delta);
                }

        // Second, calculate delta weight of kernel on this level
        for(int i=0; i<kernel.length; i++){
            // Process each kernel
            TensorMath.dconvWeight(delta.get(i), output_prev, kernel[i], convolution_stride, kernel_delta[i]);
            TensorMath.mult(kernel_delta[i], learning_rate, kernel_delta[i]);
                // The code with 1 extra tab is for momentum
                TensorMath.mult(momentum_kernel[i], momentum_factor, temp_tensor_kernel);
                TensorMath.add(kernel_delta[i], temp_tensor_kernel, kernel_delta[i]);
            TensorMath.add(kernel[i], kernel_delta[i], kernel[i]);
                // The code with 1 extra tab is for momentum
                TensorMath.copy(kernel_delta[i], momentum_kernel[i]);
        }

        // Thrid, calculate delta weight of bias on this level
        for(int i=0; i<output.data.length; i++){
            bias_delta.data[i] = MatrixMath.sum(delta.get(i));
        }
        VectorMath.mult(bias_delta, learning_rate, bias_delta);
            // The code with 1 extra indent is for momentum
            VectorMath.mult(momentum_bias, momentum_factor, temp_vector);
            VectorMath.add(bias_delta, temp_vector, bias_delta);
        VectorMath.add(bias, bias_delta, bias);
            // The code with 1 extra indent is for momentum
            VectorMath.copy(bias_delta, momentum_bias);
    }

    public void calcError(Vector correct){
        throw new RuntimeException("Can not calculate error on ConvolutionLayer, currently only supports FullyConnectedLayer");
    }
}
