package cs838;

import java.util.*;

public abstract class Network{

    public static enum NetworkType {FullyConnected, Convolution};
    public static enum DataType {Train, Tune, Test};

    private NetworkType network_type;

    // a pointer which will point to one of the following three label vectors
    protected Vector label;
    protected Vector label_train;
    protected Vector label_tune;
    protected Vector label_test;

    // No matter FullyConnectedNetwork or CNN,
    // the last layer of the network should be a FullyConnectedLayer,
    // and the output of network should be a vector (using one-hot encoding)
    protected Vector network_output;
    protected Vector correct_answer;

    // best_layers record the best parameter setting during training
    // During test, reset the parameters in all_layers to be best_layers
    protected ArrayList<Layer> all_layers;
    protected ArrayList<Layer> best_layers;

    // Parameters for network
    private int early_stopping_factor;

    public Network(NetworkType t){
        network_type = t;
        early_stopping_factor = 5;

        label_train = new Vector();
        label_tune = new Vector();
        label_test = new Vector();
    }

    public void configureFromFile(String filename){
        ArrayList<HashMap<String, Integer>> configs = ConfigParser.loadConfFromJSON(filename);
        for(HashMap<String, Integer> conf : configs){
            addLayer(conf);
        }
    }

    public void initBase(int outputDimension){
        network_output = new Vector(outputDimension);
        correct_answer = new Vector(outputDimension);

        all_layers = new ArrayList<Layer>();
        best_layers = new ArrayList<Layer>();
    }

    public void addLayerBase(HashMap<String, Integer> configure){
        Layer layer1 = null;
        Layer layer2 = null;
        Integer type = configure.get("Type");
        if(type == Layer.LayerType.FullyConnected.ordinal()){
            // Number of hidden units
            Integer hidden = configure.get("Hidden");
            // Default has a bias unit
            boolean bias = true;
            if(configure.containsKey("Bias") && (configure.get("Bias") == 0))
                bias = false;
            // Default activation type is ReLU
            FullyConnectedLayer.ActivationType act = FullyConnectedLayer.ActivationType.Relu;
            if(configure.containsKey("Activate") && (configure.get("Activate") == FullyConnectedLayer.ActivationType.Sigmoid.ordinal()))
                act = FullyConnectedLayer.ActivationType.Sigmoid;

            layer1 = new FullyConnectedLayer(hidden, bias, act);
            layer2 = new FullyConnectedLayer(hidden, bias, act);
            if(all_layers.size() == 0){
                int inputDim = configure.get("Input");
                layer1.layerAsBegin(1, inputDim);
                layer2.layerAsBegin(1, inputDim);
            }
            else{
                layer1.layerAsMiddle(all_layers.get(all_layers.size()-1));
                layer2.layerAsMiddle(best_layers.get(best_layers.size()-1));
            }
        }
        else{
            if(type == Layer.LayerType.Convolution.ordinal()){
                int stride = configure.get("Stride");
                int kernSize = configure.get("KernSize");
                int numKern = configure.get("NumKernel");
                layer1 = new ConvolutionLayer(stride, kernSize, numKern);
                layer2 = new ConvolutionLayer(stride, kernSize, numKern);
            }
            else if(type == Layer.LayerType.Polling.ordinal()){
                int stride = configure.get("Stride");
                layer1 = new PollingLayer(stride);
                layer2 = new PollingLayer(stride);
            }
            else if(type == Layer.LayerType.Normalize.ordinal()){
                layer1 = new NormalizeLayer();
                layer2 = new NormalizeLayer();
            }
            else{
                throw new RuntimeException("Unknown layer type");
            }

            if(all_layers.size() == 0){
                int channel = configure.get("Channel");
                int inputDim = configure.get("Input");
                layer1.layerAsBegin(channel, inputDim);
                layer2.layerAsBegin(channel, inputDim);
            }
            else{
                layer1.layerAsMiddle(all_layers.get(all_layers.size()-1));
                layer2.layerAsMiddle(best_layers.get(best_layers.size()-1));
            }
        }
        all_layers.add(layer1);
        best_layers.add(layer2);
    }

    public void initializeLayersBase(){
        // Initialize each layer
        for(int i=0; i<all_layers.size(); i++){
            Layer next = (i+1 < all_layers.size()) ? all_layers.get(i+1) : null;
            all_layers.get(i).init(next);
        }
        // initialize the output of this network
        Layer last = all_layers.get(all_layers.size()-1);
        if(last.layer_type != Layer.LayerType.FullyConnected){
            throw new RuntimeException("The last layer of the network should be a FullyConnectedLayer");
        }
        FullyConnectedLayer lastLayer = (FullyConnectedLayer)last;
        network_output = lastLayer.output;
    }

    public void storeBestLayerParameter(){
        for(int i=0; i<all_layers.size(); i++){
            all_layers.get(i).copy(best_layers.get(i));
        }
    }

    public void restoreBestLayerParameter(){
        for(int i=0; i<all_layers.size(); i++){
            best_layers.get(i).copy(all_layers.get(i));
        }
    }

    public void forward(){
        for(int j=0; j<all_layers.size(); j++)
            all_layers.get(j).forward();
    }

    public void backward(int label){
        VectorMath.oneHot(correct_answer, label);
        all_layers.get(all_layers.size()-1).calcError(correct_answer);
        for(int j=all_layers.size()-1; j>=0; j--)
            all_layers.get(j).backward();
    }

    public int getAnswer(){
        return VectorMath.maxOverZero(network_output);
    }

    public void train(){
        float best_accuracy = 0.0f;
        int count = 0;
        ConvolutionLayer.disableDropout();
        FullyConnectedLayer.disableDropout();
        for(int i=0; i<50; i++){
            trainEpoch();

            float accuracy = testEpoch(DataType.Tune);
            
            String report = String.format("Tune set accuracy for %d epoch is %f", i, accuracy);
            System.out.println(report);

            // stop training when accuracy on tune dataset does not increase for a number of iterations
            if(accuracy > best_accuracy){
                storeBestLayerParameter();
                best_accuracy = accuracy;
                count = 0;
            }
            else{
                count += 1;
                if(count == early_stopping_factor)
                    break;
            }
        }
    }

    public void test(){
        restoreBestLayerParameter();
        float accuracy = testEpoch(DataType.Test);
        String report = String.format("Test set accuracy is %f", accuracy);
        System.out.println(report);
    }


    // Implement these methods in different network

    // Initialize
    public abstract void init(String filename);

    // Provide some additional information about layer configuration, then call addLayerBase
    public abstract void addLayer(HashMap<String, Integer> configure);

    // Provide some additional information about network input, then call initializeLayersBase
    public abstract void initializeLayers();

    // Train epoch
    public abstract void trainEpoch();

    // Test epoch on a certain dataset
    public abstract float testEpoch(DataType t);
}
