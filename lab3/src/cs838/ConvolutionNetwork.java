package cs838;

import java.util.*;

public class ConvolutionNetwork extends Network{

    private Tensor[] feature;
    private Tensor[] feature_train;
    private Tensor[] feature_tune;
    private Tensor[] feature_test;

    private Tensor network_input;
    
    private HashMap<String, Float> label_to_val;

    boolean need_gray;
    int image_size;

    public ConvolutionNetwork(boolean needGray, int imageSize){
        super(Network.NetworkType.Convolution);
        need_gray = needGray;
        image_size = imageSize;
    }

    public void init(String filename){
        label_to_val = ImageParser.parseLabelMapping();

        feature_train = ImageParser.loadFromDirectory(filename + "/trainset", label_to_val, label_train, need_gray, image_size);
        feature_tune = ImageParser.loadFromDirectory(filename + "/tuneset", label_to_val, label_tune, need_gray, image_size);
        feature_test = ImageParser.loadFromDirectory(filename + "/testset", label_to_val, label_test, need_gray, image_size);

        initBase(label_to_val.size());
    }

    public void addLayer(HashMap<String, Integer> configure){
        // For the first layer, set the input dimension to be the input dimension of this network
        if(all_layers.size() == 0){
            configure.put("Channel", need_gray ? 4 : 3);
            int size = feature_train[0].data[0].data.length;
            configure.put("Input", size);
        }
        addLayerBase(configure);
    }

    public void initializeLayers(){
        // initializeLayersBase will initialize the network and network_output
        // Here, we only need to init the network_input
        initializeLayersBase();
        ConvolutionLayer first = (ConvolutionLayer)all_layers.get(0);
        network_input = first.output_prev;
    }

    public void trainEpoch(){
        feature = feature_train;
        label = label_train;

        // TODO: Is this shuffle necessary?
        // Add shuffle for training data seems to make the accuracy on tuning set oscillate rapidly
        // But the accuracy on test set seems to be good
        ArrayList<Integer> order = new ArrayList<Integer>();
        for(int i=0; i<feature.length; i++)
            order.add(i);
        Collections.shuffle(order);
        for(int i=0; i<feature.length; i++){
            // Set the input network, the rest of things are handles by the base class Network
            int which = order.get(i);
            network_input.set(feature[which].data);
            forward();
            backward((int)(label.get(which)));
        }
    }

    public float testEpoch(DataType t){
        if(t == DataType.Train){
            feature = feature_train;
            label = label_train;
        }
        else if(t == DataType.Tune){
            feature = feature_tune;
            label = label_tune;
        }
        else if(t == DataType.Test){
            feature = feature_test;
            label = label_test;
        }
        int counter = 0;
        for(int i=0; i<feature.length; i++){
            // Set the input network, the rest of things are handles by the base class Network
            network_input.set(feature[i].data);
            forward();
            int answer = getAnswer();
            if(answer == (int)(label.get(i)))
                counter += 1;
        }
        return (float)(counter) / (float)(feature.length);
    }
}

