package cs838;

public class PollingLayer extends Layer{

    public int polling_stride;

    public Tensor output_prev;
    // Just grab the pointer to the next layer
    // Because it need the layer_type of the next layer during backpropagate
    public Layer next;

    public Tensor output;
    public Tensor delta;

    public PollingLayer(int stride){
        // Currently the polling layer use polling_stride = polling_kernel_size
        super(Layer.LayerType.Polling);
        polling_stride = stride;
    }

    public void copy(Layer destination){
        if(destination.layer_type != Layer.LayerType.Polling){
            throw new RuntimeException("Can only copy between two Polling layers");
        }
        PollingLayer dst = (PollingLayer)destination;
        dst.polling_stride = polling_stride;
    }

    public void layerAsMiddle(Layer prev){
        if(prev.layer_type == Layer.LayerType.Convolution){
            ConvolutionLayer prevLayer = (ConvolutionLayer)prev;
            output_prev = prevLayer.output;
        }
        else if(prev.layer_type == Layer.LayerType.Polling){
            PollingLayer prevLayer = (PollingLayer)prev;
            output_prev = prevLayer.output;
        }
        else{
            throw new RuntimeException("The previous layer of Polling can only be Convolution or Polling");
        }
        int channel = output_prev.data.length;
        int lengthInput = output_prev.data[0].data.length;
        int outputLength = MatrixMath.calcPollSize(lengthInput, polling_stride);
        output = new Tensor(channel, outputLength, outputLength);
        delta = new Tensor(channel, outputLength, outputLength);
    }

    public void layerAsBegin(int channel, int lengthInput){
        output_prev = new Tensor(channel, lengthInput, lengthInput);
        int outputLength = MatrixMath.calcPollSize(lengthInput, polling_stride);
        output = new Tensor(channel, outputLength, outputLength);
        delta = new Tensor(channel, outputLength, outputLength);
    }

    public void init(Layer n){
        // The information about previous layer has been saved by layerAsMiddle or layerAsBegin
        // So, just do some check and save the next layer
        if(n.layer_type == Layer.LayerType.FullyConnected){
            throw new RuntimeException("Can not connect Polling directly with FullyConnected");
        }
        next = n;
    }
    
    public void forward(){
        TensorMath.poll(output_prev, polling_stride, output);
    }

    public void backward(){
        if(next.layer_type == Layer.LayerType.Normalize){
            // If the next layer is normalize, the weight between this layer and next layer is always 1
            // So, just grab the delta from next layer and resize it
            NormalizeLayer nextLayer = (NormalizeLayer)next;
            TensorMath.resize(nextLayer.delta, delta);
        }
        else if(next.layer_type == Layer.LayerType.Convolution){
            ConvolutionLayer nextLayer = (ConvolutionLayer)next;            
            // Since this is polling layer, like it has a linear activation function, so calc delta only use prev delta and weight
            Tensor next_delta = nextLayer.delta;
            Tensor[] next_kernel = nextLayer.kernel;
            Vector next_bias = nextLayer.bias;
            int next_convolution_stride = nextLayer.convolution_stride;

            TensorMath.zero(delta);
            for(int i=0; i<next_kernel.length; i++){
                TensorMath.dconvDeltaAdd(next_delta.get(i), next_kernel[i], next_convolution_stride, delta);
            }
            // Since polling layer does not have an activate function, so just sum the delta of next layer is enough
        }
        // There is no need to update weight, since each time during the forward, weight is not used 
        // Only need to propagate delta to the previous layer
    }

    public void calcError(Vector correct){
        throw new RuntimeException("Can not calculate error on PollingLayer, currently only supports FullyConnectedLayer");
    }
}
