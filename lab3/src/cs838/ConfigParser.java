package cs838;

import java.io.*;
import java.util.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONTokener;

public class ConfigParser{
    private static Integer parseLayerType(String layerType){
        switch(layerType){
            case "FullyConnected":
                return Layer.LayerType.FullyConnected.ordinal();
            case "Convolution":
                return Layer.LayerType.Convolution.ordinal();
            case "Polling":
                return Layer.LayerType.Polling.ordinal();
            case "Normalize":
                return Layer.LayerType.Normalize.ordinal();
            default:
                throw new RuntimeException("Unknown layer type");
        }
    }

    private static Integer parseActivationType(String activationType){
        switch(activationType){
            case "Sigmoid":
                return FullyConnectedLayer.ActivationType.Sigmoid.ordinal();
            case "Relu":
                return FullyConnectedLayer.ActivationType.Relu.ordinal();
            default:
                throw new RuntimeException("Unknown activation type");
        }
    }

    private static HashMap<String, Integer> jsonToMap(JSONObject jObj) throws JSONException{
        HashMap<String, Integer> mObj = new HashMap<String, Integer>();
        Iterator<?> keys = jObj.keys();
        while(keys.hasNext()){
            String key = (String)keys.next();
            Integer value;
            if(key.equals("Type"))
                value = parseLayerType((String)jObj.getString(key));
            else if(key.equals("Activate"))
                value = parseActivationType((String)jObj.getString(key));
            else
                value = jObj.getInt(key);
            mObj.put(key, value);
        }
        return mObj;
    }

    public static ArrayList<HashMap<String, Integer>> loadConfFromJSON(String filename){
        ArrayList<HashMap<String, Integer>> result = new ArrayList<HashMap<String, Integer>>();
        try{
            JSONArray allConf = new JSONArray(new JSONTokener(new FileReader(filename)));
            for(Object conf : allConf){
                JSONObject jObj = (JSONObject)conf;
                result.add(jsonToMap(jObj));
            }
        }catch(Exception e){
            System.out.println("Error parsing JSON configure");
            e.printStackTrace();
        }
        return result;
    }

}
