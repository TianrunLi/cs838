package cs838;

import java.util.*;

class Matrix {
    public float[][] data;

    public Vector get(int i){
        return new Vector(data[i]);
    }

    public float[] getv(int i){
        return data[i];
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i=0; i<data.length; i++){
            sb.append("[");
            for(int j=0; j<data[0].length-1; j++){
                sb.append(data[i][j]);
                sb.append(", ");
            }
            sb.append(data[i][data[0].length-1]);
            if(i == data.length-1)
                sb.append("]]");
            else
                sb.append("],\n");
        }
        return sb.toString();
    }

    public void resize(int dx, int dy){
        data = new float[dx][dy];
    }

    public void set(float[][] source){
        data = source;
    }

    public void set(int[][] source){
        // This is used by converting Image to Tensor
        // Image data are all normalized to [0.0f, 1.0f]
        for(int i=0; i<source.length; i++)
            for(int j=0; j<source[0].length; j++)
                data[i][j] = (float)(source[i][j] / 255.0f);
    }

    // Different construction functions
    public Matrix(float[][] source){
        data = source;
    }

    public Matrix(int dx, int dy){
        data = new float[dx][dy];
    }

    public Matrix(){
        data = null;
    }
}

public class MatrixMath{
    public static Random rnd = new Random();

    public static void add(Matrix operand1, Matrix operand2, Matrix destination){
        float[][] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[][] dst = destination.data;
        for(int i=0; i<op1.length; i++)
            for(int j=0; j<op1[0].length; j++)
                dst[i][j] = op1[i][j] + op2[i][j];
    }

    public static void incr(Matrix operand, float val){
        // Increase the value of a matrix by adding a scala
        float[][] op = operand.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                op[i][j] += val;
    }

    public static void minus(Matrix operand1, Matrix operand2, Matrix destination){
        float[][] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[][] dst = destination.data;
        for(int i=0; i<op1.length; i++)
            for(int j=0; j<op1[0].length; j++)
                dst[i][j] = op1[i][j] - op2[i][j];
    }

    public static void mult(Matrix operand, float val, Matrix destination){
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                dst[i][j] = op[i][j] * val;
    }

    public static void vmult(Matrix operand1, Matrix operand2, Matrix destination){
        float[][] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[][] dst = destination.data;
        for(int i=0; i<op1.length; i++){
            for(int j=0; j<op1[0].length; j++){
                dst[i][j] = op1[i][j] * op2[i][j];
            }
        }
    }

    public static void div(Matrix operand, float val, Matrix destination){
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                dst[i][j] = op[i][j] / val;
    }

    public static float sum(Matrix operand){
        float[][] op = operand.data;
        float sum = 0.0f;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                sum += op[i][j];
        return sum;
    }

    public static void relu(Matrix operand, Matrix destination){
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++){
            for(int j=0; j<op[0].length; j++){
                if(op[i][j] < 0.0f)
                    dst[i][j] = 0.0f;
                else
                    dst[i][j] = op[i][j];
            }
        }
    }

    public static void drelu(Matrix operand, Matrix destination){
        // TODO: Need unittest
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++){
            for(int j=0; j<op[0].length; j++){
                if(op[i][j] == 0.0f)
                    dst[i][j] = 0.0f;
                else
                    dst[i][j] = 1.0f;
            }
        }
    }

    public static void conv(Matrix operand, Matrix kernel, int stride, Matrix destination){
        // Convolution algorithm for matrix, does not consider the depth of image
        float[][] op = operand.data;
        float[][] kern = kernel.data;
        float[][] dst = destination.data;
        final int kernSize = kern.length;
        for(int i=0; i<dst.length; i++){
            for(int j=0; j<dst[0].length; j++){
                int startx = i * stride;
                int starty = j * stride;
                float sum = 0.0f;
                for(int ii=0; ii<kernSize; ii++)
                    for(int jj=0; jj<kernSize; jj++)
                        sum += op[ii+startx][jj+starty] * kern[ii][jj];
                dst[i][j] = sum;
            }
        }
    }

    public static void convAdd(Matrix operand, Matrix kernel, int stride, Matrix destination){
        // All functions ends by 'Add' means to call this function multiple times and accumulate the result on destination
        // Convolution algorithm for matrix, does not consider the depth of image
        // Accumulate the result of convolution on a destination matrix
        // This function is actually used in CNN
        float[][] op = operand.data;
        float[][] kern = kernel.data;
        float[][] dst = destination.data;
        final int kernSize = kern.length;
        for(int i=0; i<dst.length; i++){
            for(int j=0; j<dst[0].length; j++){
                int startx = i * stride;
                int starty = j * stride;
                float sum = 0.0f;
                for(int ii=0; ii<kernSize; ii++)
                    for(int jj=0; jj<kernSize; jj++)
                        sum += op[ii+startx][jj+starty] * kern[ii][jj];
                dst[i][j] += sum;
            }
        }
    }

    public static void dconvDeltaAdd(Matrix source, Matrix kernel, int stride, Matrix destination){
        // TODO: Need unittest
        // All functions ends by 'Add' means to call this function multiple times and accumulate the result on destination
        // Consider x[:,:,0] (as destination), kernel layer w0[:,:,0], and o[:,:,0] (as source)
        float[][] src = source.data;
        float[][] kern = kernel.data;
        float[][] dst = destination.data;
        int kernelDx = kern.length;
        int kernelDy = kern[0].length;
        for(int i=0; i<src.length; i++){
            for(int j=0; j<src[0].length; j++){
                int startx = i * stride;
                int starty = j * stride;
                float val = src[i][j];
                for(int ii=0; ii<kernelDx; ii++){
                    for(int jj=0; jj<kernelDy; jj++){
                        dst[startx+ii][starty+jj] += val * kern[ii][jj];
                    }
                }
            }
        }
    }

    public static void dconvWeight(Matrix delta, Matrix prev_output, Matrix kernel, int stride, Matrix destination){
        // TODO: Need unittest
        // Consider each channel of the kernel, calculate the delta weight of this channel of kernel
        float[][] dlt = delta.data;
        float[][] pout = prev_output.data;
        float[][] kern = kernel.data;
        float[][] dst = destination.data;
        MatrixMath.zero(destination);
        for(int i=0; i<dlt.length; i++){
            for(int j=0; j<dlt[0].length; j++){
                int startx = i * stride;
                int starty = j * stride;
                float val = dlt[i][j];
                for(int ii=0; ii<kern.length; ii++){
                    for(int jj=0; jj<kern[0].length; jj++){
                        dst[ii][jj] += val * pout[startx+ii][starty+jj];
                    }
                }
            }
        }
    }

    public static void poll(Matrix operand, int stride, Matrix destination){
        // Max pooling for matrix, does not consider the depth of image
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<dst.length; i++){
            for(int j=0; j<dst[0].length; j++){
                int startx = i * stride;
                int starty = j * stride;
                float max = op[startx][starty];
                for(int ii=startx; ii<startx+stride; ii++)
                    for(int jj=starty; jj<starty+stride; jj++)
                        if(op[ii][jj] > max)
                            max = op[ii][jj];
                dst[i][j] = max;
            }
        }
    }

    public static void dpoll(Matrix operand, Matrix source, int stride, Matrix destination){
        // TODO: Need unittest
        // source is the delta of next layer, destination is the delta of current layer
        // operand is the output of current layer, which is used at polling
        float[][] op = operand.data;
        float[][] src = source.data;
        float[][] dst = destination.data;
        for(int i=0; i<src.length; i++){
            for(int j=0; j<src[0].length; j++){
                int startx = i * stride;
                int starty = j * stride;
                // Find, in a stride*stride polling square, which one is the largest?
                float max = op[startx][starty];
                int maxx = startx;
                int maxy = starty;
                for(int ii=startx; ii<startx+stride; ii++)
                    for(int jj=starty; jj<starty+stride; jj++)
                        if(op[ii][jj] > max){
                            max = op[ii][jj];
                            maxx = ii;
                            maxy = jj;
                        }
                // Mark all value in the stride*stride polling square as 0.0f, only the largest one as val
                float val = src[i][j];
                for(int ii=startx; ii<startx+stride; ii++)
                    for(int jj=starty; jj<starty+stride; jj++)
                        dst[ii][jj] = 0.0f;
                dst[maxx][maxy] = val;
            }
        }
    }

    public static int calcPollSize(int operandLength, int stride){
        // This function calculates the size of destination matrix for polling
        // Only support non-overlapping polling, when S = F
        // Throw an exception if not divisible
        // (W - F) / S + 1, and F = S, so W / S
        if(operandLength % stride != 0){
            String parameter = String.format("Require divisible in polling. W: %d, S: %d", operandLength, stride);
            throw new RuntimeException(parameter);
        }
        return operandLength / stride;
    }

    public static int calcConvSize(int operandLength, int kernelLength, int stride){
        // This function calculates the size of destination matrix for convolution
        // For simplicity, does not consider zero padding, P = 0
        // Throw an exception if not divisible
        // (W - F + 2P) / S + 1, where P = 0 (no padding)
        if((operandLength - kernelLength) % stride != 0){
            String parameter = String.format("Require divisible in convolution. W: %d, F: %d, S: %d", operandLength, kernelLength, stride);
            throw new RuntimeException(parameter);
        }
        return (operandLength - kernelLength) / stride + 1;
    }

    public static void multm(Matrix operand1, Matrix operand2, Matrix destination){
        // Multiple two matrix
        // Note that destination can not be the same matrix as operand
        float[][] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[][] dst = destination.data;
        for(int i=0; i<dst.length; i++){
            for(int j=0; j<dst[0].length; j++){
                float sum = 0.0f;
                for(int k=0; k<op2.length; k++)
                    sum += op1[i][k] * op2[k][j];
                dst[i][j] = sum;
            }
        }
    }

    public static void lprod(Matrix operand1, Vector operand2, Vector destination){
        // Multiply a matrix with a vector
        // Matrix(a*b) * Vector(b) = Vector(a)
        float[][] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<op1.length; i++){
            float sum = 0.0f;
            for(int j=0; j<op2.length; j++)
                sum += op1[i][j] * op2[j];
            dst[i] = sum;
        }
    }

    public static void rprod(Vector operand1, Matrix operand2, Vector destination){
        // Multiply a vector with a matrix
        // Vector(a) * Matrix(a*b) = Vector(b)
        float[] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<op2[0].length; i++){
            float sum = 0.0f;
            for(int j=0; j<op1.length; j++)
                sum += op1[j] * op2[j][i];
            dst[i] = sum;
        }
    }

    public static void rand(Matrix operand, float limit){
        // randomize matrix in range [-limit, limit]
        float[][] op = operand.data;
        float divide = 1.0f / limit / 2.0f;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                op[i][j] = rnd.nextFloat() / divide - limit;
    }

    public static void range(Matrix operand, float start, float delta){
        // initialize matrix to be a arithmetic sequence
        float[][] op = operand.data;
        int counter = 0;
        for(int i=0; i<op.length; i++){
            for(int j=0; j<op[0].length; j++){

                op[i][j] = start + counter * delta;
                counter++;
            }
        }
    }

    public static void range(Matrix operand){
        range(operand, 0.0f, 1.0f);
    }

    public static void zero(Matrix operand){
        float[][] op = operand.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                op[i][j] = 0.0f;
    }

    public static void mask(Matrix operand, float drop_rate){
        // Set 'drop_rate'% of the data to be 1.0f, others 0.0f
        // Means 'drop_rate'% of the neuron is active
        float[][] op = operand.data;
        for(int i=0; i<op.length; i++){
            for(int j=0; j<op[0].length; j++){
                float val = rnd.nextFloat();
                if(val < drop_rate)
                    op[i][j] = 1.0f;
                else
                    op[i][j] = 0.0f;
            }
        }
    }

    public static void copy(Matrix operand, Matrix destination){
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                dst[i][j] = op[i][j];
    }

    public static void resize(Matrix operand, int start, Vector destination){
        // This function is used by TensorMath.resize
        // Start is the start position in the destination vector
        float[][] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++){
            int iter = start + i * op[0].length;
            for(int j=0; j<op[0].length; j++)
                dst[iter+j] = op[i][j];
        }
    }

    public static void resize(Vector operand, int start, Matrix destination){
        // Start is the start position in the operand vector
        float[] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<dst.length; i++){
            int iter = start + i * dst[0].length;
            for(int j=0; j<dst[0].length; j++)
                dst[i][j] = op[iter+j];
        }
    }

    public static int size(Matrix operand){
        // Size of a matrix is the number of elements in this matrix
        float[][] op = operand.data;
        return op.length * op[0].length;
    }

    public static Matrix empty_like(Matrix operand){
        float[][] op = operand.data;
        return new Matrix(op.length, op[0].length);
    }

    public static void main(String[] args){
        /*
        Matrix m1 = new Matrix(3, 3);
        MatrixMath.rand(m1, 30.0f);
        System.out.println(m1);

        m1 = new Matrix(3, 3);
        MatrixMath.range(m1);
        m1.data[0][0] = 1.0f;
        System.out.println(m1);
        System.out.println("");

        Vector v1 = new Vector(3);
        VectorMath.range(v1);
        v1.data[0] = 1.0f;
        System.out.println(v1);
        System.out.println("");

        Vector res = new Vector();
        res.empty_like(v1);
        MatrixMath.lprod(m1, v1, res);
        System.out.println(res);
        System.out.println("");

        System.out.println(m1);
        Vector v = m1.get(0);
        v.data[0] = 100;
        System.out.println(m1);
        */


        // test of polling
        /*
        Matrix origin = new Matrix(6,6);
        MatrixMath.range(origin);
        int size1 = MatrixMath.calcPollSize(origin.data.length, 2);
        int size2 = MatrixMath.calcPollSize(origin.data.length, 3);
        Matrix result1 = new Matrix(size1, size1);
        Matrix result2 = new Matrix(size2, size2);
        MatrixMath.poll(origin, 2, result1);
        MatrixMath.poll(origin, 3, result2);

        System.out.println(origin);
        System.out.println("");
        System.out.println(result1);
        System.out.println("");
        System.out.println(result2);
        */

        float[][] data1 = { {0,0,0,0,0,0,0},
                            {0,0,1,2,2,0,0},
                            {0,1,2,1,1,1,0},
                            {0,1,1,0,1,2,0},
                            {0,1,1,0,2,1,0},
                            {0,1,1,0,1,0,0},
                            {0,0,0,0,0,0,0}};

        float[][] kern11 = {{0,0,-1},
                            {-1,-1,1},
                            {1,1,-1}};

        Matrix input = new Matrix(data1);
        Matrix kern = new Matrix(kern11);
        Matrix result = new Matrix(3,3);
        MatrixMath.conv(input, kern, 2, result);
        System.out.println(result);
    }
}
