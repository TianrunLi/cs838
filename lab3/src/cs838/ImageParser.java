package cs838;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import javax.imageio.ImageIO;

import java.util.*;
import java.io.*;

public class ImageParser{
    
    // Work as an adaptor between Instance class provided by Jude and the Tensor class by myself
    // Provide Array<Tensor> as the interface to dataset

    public static HashMap<String, Float> parseLabelMapping(){
        HashMap<String, Float> ret = new HashMap<String, Float>(){
            {
                put("airplanes", 0.0f);
                put("butterfly", 1.0f);
                put("flower", 2.0f);
                put("grand_piano", 3.0f);
                put("starfish", 4.0f);
                put("watch", 5.0f);
            }
        };
        return ret;
    }

    public static Tensor[] loadFromDirectory(String directoryName, HashMap<String, Float> labelMapping, Vector labelArr, boolean needGray, int imageSize){
        ArrayList<Float> labelTemp = new ArrayList<Float>();
        ArrayList<Tensor> featureArr = new ArrayList<Tensor>();
        File dir = new File(directoryName);
        for(File file : dir.listFiles()){
            if(!file.isFile() || !file.getName().endsWith(".jpg")) {
                continue;
            }
            BufferedImage img = null, scaledBI = null;
            try {
                // load in all images
                img = ImageIO.read(file);
                // every image's name is in such format:
                // label_image_XXXX(4 digits) though this code could handle more than 4 digits.
                String name = file.getName();
                int locationOfUnderscoreImage = name.indexOf("_image");                
                // Resize the image if requested.  Any resizing allowed, but should really be one of 8x8, 16x16, 32x32, or 64x64 (original data is 128x128).
                if (imageSize != 128) {
                    scaledBI = new BufferedImage(imageSize, imageSize, BufferedImage.TYPE_INT_RGB);
                    Graphics2D g = scaledBI.createGraphics();
                    g.drawImage(img, 0, 0, imageSize, imageSize, null);
                    g.dispose();
                }
                Instance instance = new Instance(scaledBI == null ? img : scaledBI, name.substring(0, locationOfUnderscoreImage));

                // Adapt Instance class to Tensor class
                labelTemp.add(labelMapping.get(instance.getLabel()));
                Tensor t = new Tensor();
                t.fromImage(instance, needGray);
                featureArr.add(t);
            } catch (IOException e) {
                System.err.println("Error: cannot load in the image file");
                System.exit(1);
            }
        }
        float[] vector = new float[labelTemp.size()];
        for(int i=0; i<labelTemp.size(); i++)
            vector[i] = labelTemp.get(i);
        labelArr.set(vector);
        String report = String.format("Load %d examples %s gray image", featureArr.size(), needGray ? "with" : "without");
        System.out.println(report);
        return featureArr.toArray(new Tensor[featureArr.size()]);
    }

    public static void main(String[] args){
        HashMap<String, Float> labelMapping = parseLabelMapping();
        ArrayList<Tensor> feature = new ArrayList<Tensor>();
        Vector label = new Vector();
        boolean needGray = true;

        loadFromDirectory("images/tuneset", labelMapping, label, needGray, 128);
        Tensor img = feature.get(0);
        System.out.println(img);
    }
}
