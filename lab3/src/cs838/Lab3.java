package cs838;

import java.util.*;

public class Lab3 {

    private Network network;

    public Lab3(){}

    public void lab2(String dataFile, String confFile){
        network = new FullyConnectedNetwork();
        network.init(dataFile);
        network.configureFromFile(confFile);
        network.initializeLayers();
        network.train();
        network.test();
    }

    public void lab3(String dataFile, String confFile){
        network = new ConvolutionNetwork(true, 32);
        network.init(dataFile);
        network.configureFromFile(confFile);
        network.initializeLayers();
        network.train();
        network.test();   
    }

    public static void main(String[] args) {
        Lab3 nn = new Lab3();
        if(args.length != 3){
            System.out.println("java cs838.Lab3 <which-lab> <data-source> <network-configuration-file>");
            return;
        }
        if(args[0].equals("lab2"))
            nn.lab2(args[1], args[2]);
        else if(args[0].equals("lab3"))
            nn.lab3(args[1], args[2]);
    }
}
