package cs838;

import java.util.*;

class Tensor {
    public Matrix[] data;

    public Matrix get(int i){
        return data[i];
    }

    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for(int i=0; i<data.length; i++){
            builder.append(data[i].toString());
            if(i == data.length-1)
                builder.append("]");
            else
                builder.append(",\n");
        }
        return builder.toString();
    }

    public void resize(int dx, int dy, int dz){
        data = new Matrix[dx];
        for(int i=0; i<dx; i++)
            data[i] = new Matrix(dy, dz);
    }

    public void set(Matrix[] source){
        data = source;
    }

    public void fromImage(Instance source, boolean needGray){
        // Initialize a Tensor from image data
        if(needGray)
            data = new Matrix[4];
        else
            data = new Matrix[3];
        for(int i=0; i<data.length; i++)
            data[i] = new Matrix(source.getWidth(), source.getHeight());
        data[0].set(source.getRedChannel());
        data[1].set(source.getGreenChannel());
        data[2].set(source.getBlueChannel());
        if(needGray)
            data[3].set(source.getGrayImage());
    }

    // Different construction funcsions
    public Tensor(Matrix[] source){
        data = source;
    }

    public Tensor(int dx, int dy, int dz){
        data = new Matrix[dx];
        for(int i=0; i<data.length; i++){
            data[i] = new Matrix(dy, dz);
        }
    }

    public Tensor(){
        data = null;
    }
}

public class TensorMath{
    // Definition of all these convolution operation:
    // http://cs231n.github.io/convolutional-networks/

    public static void rand(Tensor operand, float rand_limit){
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.rand(operand.get(i), rand_limit);
        }
    }

    public static void zero(Tensor operand){
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.zero(operand.get(i));
        }
    }

    public static void mask(Tensor operand, float drop_rate){
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.mask(operand.get(i), drop_rate);
        }
    }

    public static void resize(Tensor operand, Vector destination){
        // Used in normalize layer, since destination vector has bias = 1.0f
        // so the copy size is determined by operand
        int matrixSize = MatrixMath.size(operand.data[0]);
        int iter = 0;
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.resize(operand.get(i), iter, destination);
            iter += matrixSize;
        }
    }

    public static void resize(Vector operand, Tensor destination){
        // Used in normalize layer, since destination vector has bias = 1.0f
        // so the copy size is determined by operand
        int matrixSize = MatrixMath.size(destination.data[0]);
        int iter = 0;
        for(int i=0; i<destination.data.length; i++){
            MatrixMath.resize(operand, iter, destination.data[i]);
            iter += matrixSize;
        }
    }

    public static void add(Tensor operand1, Tensor operand2, Tensor destination){
        for(int i=0; i<operand1.data.length; i++){
            MatrixMath.add(operand1.get(i), operand2.get(i), destination.get(i));
        }
    }

    public static void mult(Tensor operand, float val, Tensor destination){
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.mult(operand.get(i), val, destination.get(i));
        }
    }

    public static void vmult(Tensor operand1, Tensor operand2, Tensor destination){
        for(int i=0; i<operand1.data.length; i++){
            MatrixMath.vmult(operand1.get(i), operand2.get(i), destination.get(i));
        }
    }

    public static void div(Tensor operand, float val, Tensor destination){
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.div(operand.get(i), val, destination.get(i));
        }
    }

    public static void copy(Tensor operand, Tensor destination){
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.copy(operand.get(i), destination.get(i));
        }
    }

    public static void relu(Tensor operand, Tensor destination){
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.relu(operand.get(i), destination.get(i));
        }
    }

    public static void drelu(Tensor operand, Tensor destination){
        // TODO: Need unittest
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.drelu(operand.get(i), destination.get(i));
        }
    }

    public static void convBias(Tensor operand, Vector bias){
        // Add a bias using a vector
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.incr(operand.get(i), bias.get(i));
        }
    }

    public static void conv(Tensor operand, Tensor[] kernel, int stride, Tensor destination){
        // Apply convolution using a series of kernel
        for(int i=0; i<kernel.length; i++){
            Matrix d = destination.get(i);
            Tensor kern = kernel[i];
            // First zero the matrix, then apply convAdd to perform convolution
            MatrixMath.zero(d);
            for(int j=0; j<operand.data.length; j++)
                MatrixMath.convAdd(operand.get(j), kern.get(j), stride, d);
        }
    }

    public static void dconvDeltaAdd(Matrix source, Tensor kernel, int stride, Tensor destination){
        // TODO: Need unittest
        // All functions ends by 'Add' means to call this function multiple times and accumulate the result on destination
        // Consider one kernel,
        // source is one slice of delta in next layer, kernel is one of the kernel, destination is the delta of current level
        // Delta is computed by an accumulation on destination Tensor
        // Actually, this is the same as flip 180 degree of kernel and conv back
        for(int i=0; i<kernel.data.length; i++){
            MatrixMath.dconvDeltaAdd(source, kernel.get(i), stride, destination.get(i));
        }
    }

    public static void dconvWeight(Matrix delta, Tensor prev_output, Tensor kernel, int stride, Tensor destination){
        // TODO: Need unittest
        for(int i=0; i<kernel.data.length; i++){
            // Consider each channel of the kernel
            MatrixMath.dconvWeight(delta, prev_output.get(i), kernel.get(i), stride, destination.get(i));
        }
    }

    public static void poll(Tensor operand, int stride, Tensor destination){
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.poll(operand.get(i), stride, destination.get(i));
        }
    }

    public static void dpoll(Tensor operand, Tensor source, int stride, Tensor destination){
        // TODO: Need unittest
        // Used in backpropagation of delta in polling layer
        // operand is the Tensor need to be polled, source is the delta of next layer, destination is the delta of current layer
        for(int i=0; i<operand.data.length; i++){
            MatrixMath.dpoll(operand.get(i), source.get(i), stride, destination.get(i));
        }
    }

    public static int size(Tensor operand){
        int matrixSize = MatrixMath.size(operand.data[0]);
        return operand.data.length * matrixSize;
    }

    public static void main(String[] args){
        float[][] data1 = { {0,0,0,0,0,0,0},
                            {0,2,0,2,1,1,0},
                            {0,0,0,0,1,0,0},
                            {0,2,1,2,2,2,0},
                            {0,2,2,0,1,0,0},
                            {0,0,1,0,1,2,0},
                            {0,0,0,0,0,0,0}};

        float[][] data2 = { {0,0,0,0,0,0,0},
                            {0,1,0,2,2,1,0},
                            {0,2,1,0,1,1,0},
                            {0,0,0,2,0,0,0},
                            {0,1,2,2,2,0,0},
                            {0,1,1,1,2,2,0},
                            {0,0,0,0,0,0,0}};

        float[][] data3 = { {0,0,0,0,0,0,0},
                            {0,2,1,2,0,1,0},
                            {0,1,2,1,0,2,0},
                            {0,2,1,2,1,2,0},
                            {0,0,2,2,2,1,0},
                            {0,1,2,0,1,1,0},
                            {0,0,0,0,0,0,0}};

        float[][] kern11 = {{0,0,-1},
                            {-1,-1,1},
                            {1,1,-1}};

        float[][] kern12 = {{-1,-1,0},
                            {1,0,0},
                            {1,0,1}};

        float[][] kern13 = {{0,1,0},
                            {-1,0,0},
                            {-1,-1,-1}};

        float[][] kern21 = {{-1,1,1},
                            {-1,0,0},
                            {0,1,-1}};

        float[][] kern22 = {{1,-1,1},
                            {1,1,-1},
                            {0,0,1}};

        float[][] kern23 = {{1,1,-1},
                            {1,1,0},
                            {1,0,0}};

        float[] data_bias = {1.0f, 0.0f};


        Tensor input = new Tensor(3,6,6);
        input.get(0).set(data1);
        input.get(1).set(data2);
        input.get(2).set(data3);

        Tensor[] kernel = new Tensor[2];
        for(int i=0; i<2; i++)
            kernel[i] = new Tensor(3,3,3);
        kernel[0].get(0).set(kern11);
        kernel[0].get(1).set(kern12);
        kernel[0].get(2).set(kern13);
        kernel[1].get(0).set(kern21);
        kernel[1].get(1).set(kern22);
        kernel[1].get(2).set(kern23);

        Vector bias = new Vector(data_bias);

        Tensor result = new Tensor(2,3,3);

        TensorMath.conv(input, kernel, 2, result);
        TensorMath.convBias(result, bias);
        //System.out.println(result);

        Vector resize = new Vector(27);
        TensorMath.resize(kernel[0], resize);
        System.out.println(resize);

        Tensor resize_back = new Tensor(3,3,3);
        TensorMath.resize(resize, resize_back);
        System.out.println(resize_back);
    }
}
