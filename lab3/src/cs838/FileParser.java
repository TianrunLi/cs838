package cs838;

import java.io.*;
import java.util.*;

public class FileParser {
    private static void parseHeader(String filename, HashMap<String, Integer> featureToVal, HashMap<String, Float> labelToVal){
            labelToVal.put("_", new Float(0.0));
            labelToVal.put("e", new Float(1.0));
            labelToVal.put("h", new Float(2.0));
            featureToVal.put("Z", new Integer(0));
            try{
                BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
                String content;
                while((content = reader.readLine()) != null){
                    content = content.trim();
                    if(content.length() == 0 || content.startsWith("#"))
                        continue;
                    if(content.equals("<>") || content.equals("end") || content.equals("<end>"))
                        continue;
                    String name = content.split(" ")[0].trim();
                    if(!featureToVal.containsKey(name))
                        featureToVal.put(name, new Integer(featureToVal.size()));
                }
                reader.close();
            }catch(IOException e){
                e.printStackTrace();
            }
    }

    public static void loadFromFile(String filename, Matrix featureArr, Vector labelArr,
                                    HashMap<String, Integer> featureToVal, HashMap<String, Float> labelToVal, int windowSize){
        if(featureToVal.size() == 0 && labelToVal.size() == 0)
            parseHeader(filename, featureToVal, labelToVal);
        ArrayList<float[]> feature = new ArrayList<float[]>();
        ArrayList<Float> label = new ArrayList<Float>();
        try{
            BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
            String content = null;
            int padding = (windowSize - 1) / 2;
            while(true){
                if(content == null || !content.equals("<>")){
                    content = reader.readLine();
                    if(content == null)
                        break;
                }
                content = content.trim();
                if(content.length() == 0 || content.startsWith("#"))
                    continue;
                if(content.equals("<>")){
                    // setup the beginning with padding
                    int featureCount = featureToVal.size();
                    // input vector contains a 1.0 for learning bias
                    float[] array = new float[windowSize * featureCount + 1];
                    for(int i=0; i<array.length; i++)
                        array[i] = 0.0f;
                    array[windowSize * featureCount] = 1.0f;
                    for(int i=0; i<padding; i++)
                        array[featureCount * i + featureToVal.get("Z")] = 1.0f;
                    for(int i=padding; i<windowSize; i++){
                        // here assume the length of protein sequence is at least padding + 1
                        content = reader.readLine().trim();
                        String[] lists = content.split(" ");
                        array[featureCount * i + featureToVal.get(lists[0])] = 1.0f;
                        label.add(labelToVal.get(lists[1]));
                    }
                    float[] f = array.clone();
                    feature.add(f);
                    // build features for each amino acid
                    while((content = reader.readLine()) != null){
                        content = content.trim();
                        if(content.equals("end") || content.equals("<end>") || content.equals("<>"))
                            break;
                        for(int i=featureCount; i<windowSize * featureCount; i++)
                            array[i - featureCount] = array[i];
                        for(int i=(windowSize-1) * featureCount; i<windowSize * featureCount; i++)
                            array[i] = 0.0f;
                        String[] lists = content.split(" ");
                        array[(windowSize-1) * featureCount + featureToVal.get(lists[0])] = 1.0f;
                        label.add(labelToVal.get(lists[1]));
                        f = array.clone();
                        feature.add(f);
                    }
                    // setup the ending with padding
                    for(int i=0; i<padding; i++){
                        for(int j=featureCount; j<windowSize * featureCount; j++)
                            array[j - featureCount] = array[j];
                        for(int j=(windowSize-1) * featureCount; j<windowSize * featureCount; j++)
                            array[j] = 0.0f;
                        array[(windowSize-1) * featureCount + featureToVal.get("Z")] = 1.0f;
                        f = array.clone();
                        feature.add(f);
                    }
                }
            }
            reader.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        float[][] matrix = new float[feature.size()][];
        for(int i=0; i<feature.size(); i++)
            matrix[i] = feature.get(i);
        float[] vector = new float[label.size()];
        for(int i=0; i<label.size(); i++)
            vector[i] = label.get(i);
        featureArr.set(matrix);
        labelArr.set(vector);
    }

    public static void splitTrainTuneTest(String filename){
        try{
            BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
            BufferedWriter train = new BufferedWriter(new FileWriter(new File(filename + ".train")));
            BufferedWriter tune = new BufferedWriter(new FileWriter(new File(filename + ".tune")));
            BufferedWriter test = new BufferedWriter(new FileWriter(new File(filename + ".test")));
            BufferedWriter writer;
            String content = null;
            int counter = 0;
            while(true){
                if(content == null || !content.equals("<>")){
                    content = reader.readLine();
                    if(content == null)
                        break;
                }
                content = content.trim();
                if(content.length() == 0 || content.startsWith("#"))
                    continue;
                if(content.equals("<>")){
                    counter++;
                    if(counter % 5 == 0)
                        writer = tune;
                    else if(counter > 1 && counter % 5 == 1)
                        writer = test;
                    else
                        writer = train;
                    writer.write("<>\n");
                    while((content = reader.readLine()) != null){
                        content = content.trim();
                        if(content.equals("<>"))
                            break;
                        writer.write(content);
                        writer.write("\n");
                        if(content.equals("end") || content.equals("<end>"))
                            break;
                    }
                }
            }
            test.close();
            tune.close();
            train.close();
            reader.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
