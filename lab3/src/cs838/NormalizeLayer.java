package cs838;

public class NormalizeLayer extends Layer{

    // Normalize layer connect tensor with vector
    public Tensor output_prev;
    public Vector delta_next;
    public Matrix weight_next;

    public Vector output;
    public Vector delta;

    public NormalizeLayer(){
        super(Layer.LayerType.Normalize);
    }

    public void copy(Layer destination){
        if(destination.layer_type != Layer.LayerType.Normalize){
            throw new RuntimeException("Can only copy between two Normalize layers");
        }
        // Don't need to copy any data in this layer
        // Because it only works as a adapter
    }

    public void layerAsMiddle(Layer prev){
        // If normalize layer is the first layer
        if(prev.layer_type == Layer.LayerType.Convolution){
            ConvolutionLayer prevLayer = (ConvolutionLayer)prev;
            output_prev = prevLayer.output;
        }
        else if(prev.layer_type == Layer.LayerType.Polling){
            PollingLayer prevLayer = (PollingLayer)prev;
            output_prev = prevLayer.output;
        }
        else{
            throw new RuntimeException("The previous layer of Normalize can only be Convolution or Polling");
        }
        // Add one for bias on this level
        int tensorSize = TensorMath.size(output_prev) + 1;
        output = new Vector(tensorSize);
        output.data[tensorSize-1] = 1.0f;
        delta = new Vector(tensorSize);
    }

    public void layerAsBegin(int channel, int lengthInput){
        // Use this when NormalizeLayer is the first layer in the network
        output_prev = new Tensor(channel, lengthInput, lengthInput);
        // Add one for bias on this level
        int tensorSize = TensorMath.size(output_prev) + 1;
        output = new Vector(tensorSize);
        output.data[tensorSize-1] = 1.0f;
        delta = new Vector(tensorSize);
    }

    public void init(Layer next){
        // Connection with the previous layer is set either in layerAsBegin or layerAsMiddle
        // So, only set the next layer
        if(next.layer_type != Layer.LayerType.FullyConnected)
            throw new RuntimeException("NormalizeLayer should be followed by a FullyConnectedLayer");
        FullyConnectedLayer nextLayer = (FullyConnectedLayer)next;
        delta_next = nextLayer.delta;
        weight_next = nextLayer.weight;
    }

    public void forward(){
        // In forward pass, feed the output Tensor to next layer as vector type
        TensorMath.resize(output_prev, output);
    }

    public void backward(){
        // In backward pass, calculate the delta in vector format just like FullyConnectedLayer
        MatrixMath.rprod(delta_next, weight_next, delta);
        // You can see Normalize layer as having a linear activation function and no bias
        // And all weight is 1, so no need to update the weight, just propagate the delta
    }

    public void calcError(Vector correct){
        throw new RuntimeException("Can not calculate error on NormalizeLayer, currently only supports FullyConnectedLayer");
    }
}
