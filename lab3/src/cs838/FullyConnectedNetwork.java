package cs838;

import java.util.*;

public class FullyConnectedNetwork extends Network{

    private Matrix feature;
    private Matrix feature_train;
    private Matrix feature_tune;
    private Matrix feature_test;

    private Vector network_input;

    // Mapping from string to float value
    private HashMap<String, Integer> feature_to_val;
    private HashMap<String, Float> label_to_val;

    // Parameter for this network    
    private int window_size;

    // Input of a FullyConnectedNetwork should be a vector
    // Reset the data in this vector and start
    private int input_dimension;

    public FullyConnectedNetwork(){
        super(Network.NetworkType.FullyConnected);

        feature_train = new Matrix();
        feature_tune = new Matrix();
        feature_test = new Matrix();

        feature_to_val = new HashMap<String, Integer>();
        label_to_val = new HashMap<String, Float>();

        window_size = 17;
    }

    public void init(String filename){
        FileParser.splitTrainTuneTest(filename);
        FileParser.loadFromFile(filename + ".train", feature_train, label_train, feature_to_val, label_to_val, window_size);
        FileParser.loadFromFile(filename + ".tune", feature_tune, label_tune, feature_to_val, label_to_val, window_size);
        FileParser.loadFromFile(filename + ".test", feature_test, label_test, feature_to_val, label_to_val, window_size);

        input_dimension = window_size * feature_to_val.size() + 1;
        initBase(label_to_val.size());
    }

    public void addLayer(HashMap<String, Integer> configure){
        Integer type = configure.get("Type");
        if(type != Layer.LayerType.FullyConnected.ordinal()){
            throw new RuntimeException("FullyConnectedNetwork can only have FullyConnectedLayer");
        }
        // For the first layer, set the input dimension to be the input dimension of this network
        if(all_layers.size() == 0){
            configure.put("Input", input_dimension);
        }
        addLayerBase(configure);
    }

    public void initializeLayers(){
        // initializeLayersBase will initialize the network and network_output
        // Here, we only need to init the network_input
        initializeLayersBase();
        FullyConnectedLayer first = (FullyConnectedLayer)all_layers.get(0);
        network_input = first.output_prev;
    }

    public void trainEpoch(){
        feature = feature_train;
        label = label_train;
        for(int i=0; i<feature.data.length; i++){
            // Set the input network, the rest of things are handles by the base class Network
            network_input.set(feature.getv(i));
            forward();
            backward((int)(label.get(i)));
        }
    }

    public float testEpoch(DataType t){
        if(t == DataType.Train){
            feature = feature_train;
            label = label_train;
        }
        else if(t == DataType.Tune){
            feature = feature_tune;
            label = label_tune;
        }
        else if(t == DataType.Test){
            feature = feature_test;
            label = label_test;
        }
        int counter = 0;
        for(int i=0; i<feature.data.length; i++){
            // Set the input network, the rest of things are handles by the base class Network
            network_input.set(feature.getv(i));
            forward();
            int answer = getAnswer();
            if(answer == (int)(label.get(i)))
                counter += 1;
        }
        return (float)(counter) / (float)(feature.data.length);
    }
}
