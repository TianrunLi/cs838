package cs838;

public abstract class Layer{
    public static enum LayerType {FullyConnected, Convolution, Polling, Normalize};

    public LayerType layer_type;

    public Layer(LayerType t){
        layer_type = t;
    }

    // If I want to put this layer in the middle of network, give the previous layer to set parameters
    public abstract void layerAsMiddle(Layer prev);

    // If I want to put this layer as the begin of network, set corresponding parameters using previous layer
    public abstract void layerAsBegin(int channel, int lengthInput);

    // copy this layer
    public abstract void copy(Layer destination);

    // initialize the layer using its next layer
    public abstract void init(Layer next);

    // Forward propagation
    public abstract void forward();

    // Backward propagation
    public abstract void backward();

    // Calculate error
    public abstract void calcError(Vector correct);
}
