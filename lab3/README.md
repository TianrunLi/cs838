CS838 lab3
======

# Some limitations  
For simplicity, I do not implement padding in convolution and polling, 
so, make sure the input length is devisible, otherwise there will be an RuntimeException.  
For simplicity, polling size is set to be equal to polling stride.  

# Configure  
Configure the network structure by a JSON array.  
See examples in scripts directory.  

## Compile  
`$ ant`  

## Run Lab2 using protein dataset  
`$ ant lab2 -Darg=<path-to-lab2-configure-file>`  

Or if you want to run with java in command line  
`$ java -cp .:lib/json-20160810.jar cs838.Lab3 lab2 data/protein <path-to-lab2-configure-file>`  

## Run Lab3 using image dataset  
`$ ant lab3 -Darg=<path-to-lab3-configure-file>`  

Or if you want to run with java in command line  
`$ java -cp .:lib/json-20160810.jar cs838.Lab3 lab3 images <path-to-lab3-configure-file>`  

## TODO  
Do experiments to find a better parameter setting.  
Try different network structure.  
Use the following command to find some TODO in the code  
`$ grep -r TODO`  
