import java.io.*;
import java.util.*;

class Matrix {
    public float[][] data;

    public Vector get(int i){
        return new Vector(data[i]);
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i=0; i<data.length; i++){
            sb.append("[");
            for(int j=0; j<data[0].length-1; j++){
                sb.append(data[i][j]);
                sb.append(", ");
            }
            sb.append(data[i][data[0].length-1]);
            if(i == data.length-1)
                sb.append("]]");
            else
                sb.append("],\n");
        }
        return sb.toString();
    }

    public void set(float[][] source){
        data = source;
    }

    public void empty_like(Matrix operand){
        float[][] op = operand.data;
        data = new float[op.length][op[0].length];
    }

    // Different construction functions
    public Matrix(float[][] source){
        data = source;
    }

    public Matrix(int dx, int dy){
        data = new float[dx][dy];
    }

    public Matrix(){
        data = null;
    }
}

class MatrixMath{
    public static Random rnd = new Random();

    public static void add(Matrix operand1, Matrix operand2, Matrix destination){
        float[][] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[][] dst = destination.data;
        for(int i=0; i<op1.length; i++)
            for(int j=0; j<op1[0].length; j++)
                dst[i][j] = op1[i][j] + op2[i][j];
    }

    public static void minus(Matrix operand1, Matrix operand2, Matrix destination){
        float[][] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[][] dst = destination.data;
        for(int i=0; i<op1.length; i++)
            for(int j=0; j<op1[0].length; j++)
                dst[i][j] = op1[i][j] - op2[i][j];
    }

    public static void mult(Matrix operand, float val, Matrix destination){
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                dst[i][j] = op[i][j] * val;
    }

    public static void div(Matrix operand, float val, Matrix destination){
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                dst[i][j] = op[i][j] / val;
    }

    public static void lprod(Matrix operand1, Vector operand2, Vector destination){
        // Inner product each line of a matrix with a vector
        // Matrix(a*b) * Vector(b) = Vector(a)
        float[][] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<op1.length; i++){
            float sum = 0.0f;
            for(int j=0; j<op2.length; j++)
                sum += op1[i][j] * op2[j];
            dst[i] = sum;
        }
    }

    public static void rprod(Vector operand1, Matrix operand2, Vector destination){
        // Inner product a vector with 
        // Vector(a) * Matrix(a*b) = Vector(b)
        float[] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<op2[0].length; i++){
            float sum = 0.0f;
            for(int j=0; j<op1.length; j++)
                sum += op1[j] * op2[j][i];
            dst[i] = sum;
        }
    }

    public static void rand(Matrix operand, float limit){
        // randomize matrix in range [-limit, limit]
        float[][] op = operand.data;
        float divide = 1.0f / limit / 2.0f;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                op[i][j] = rnd.nextFloat() / divide - limit;
    }

    public static void range(Matrix operand, float start, float delta){
        // initialize matrix to be a arithmetic sequence
        float[][] op = operand.data;
        int counter = 0;
        for(int i=0; i<op.length; i++){
            for(int j=0; j<op[0].length; j++){
                op[i][j] = start + counter * delta;
                counter++;
            }
        }
    }

    public static void range(Matrix operand){
        range(operand, 0.0f, 1.0f);
    }

    public static void zero(Matrix operand){
        float[][] op = operand.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                op[i][j] = 0.0f;
    }

    public static void copy(Matrix operand, Matrix destination){
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                dst[i][j] = op[i][j];
    }

    public static void main(String[] args){
        Matrix m1 = new Matrix(3, 3);
        MatrixMath.rand(m1, 30.0f);
        System.out.println(m1);

        m1 = new Matrix(3, 3);
        MatrixMath.range(m1);
        m1.data[0][0] = 1.0f;
        System.out.println(m1);
        System.out.println("");

        Vector v1 = new Vector(3);
        VectorMath.range(v1);
        v1.data[0] = 1.0f;
        System.out.println(v1);
        System.out.println("");

        Vector res = new Vector();
        res.empty_like(v1);
        MatrixMath.lprod(m1, v1, res);
        System.out.println(res);        
        System.out.println("");

        System.out.println(m1);
        Vector v = m1.get(0);
        v.data[0] = 100;
        System.out.println(m1);
    }
}

class Vector{
    public float[] data;

    public float get(int i){
        return data[i];
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i=0; i<data.length-1; i++){
            sb.append(data[i]);
            sb.append(", ");
        }
        sb.append(data[data.length-1]);
        sb.append("]");
        return sb.toString();
    }

    public void set(float[] source){
        data = source;
    }

    public void empty_like(Vector operand){
        data = new float[operand.data.length];
    }

    // Different constructors
    public Vector(float[] source){
        data = source;
    }

    public Vector(int sz){
        data = new float[sz];
    }

    public Vector(){
        data = null;
    }
}

class VectorMath{
    public static void add(Vector operand1, Vector operand2, Vector destination){
        float[] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<dst.length; i++)
            dst[i] = op1[i] + op2[i];
    }

    public static void minus(Vector operand1, Vector operand2, Vector destination){
        float[] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<dst.length; i++)
            dst[i] = op1[i] - op2[i];
    }
    
    public static void mult(Vector operand, float val, Vector destination){
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++)
            dst[i] = op[i] * val;
    }

    public static void vmult(Vector operand1, Vector operand2, Vector destination){
        float[] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<op1.length; i++)
            dst[i] = op1[i] * op2[i];
    }

    public static void div(Vector operand, float val, Vector destination){
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++)
            dst[i] = op[i] / val;
    }

    public static void cprod(Vector operand1, Vector operand2, Matrix destination){
        // cross product of two vector
        float[] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[][] dst = destination.data;
        for(int i=0; i<op1.length; i++)
            for(int j=0; j<op2.length; j++)
                dst[i][j] = op1[i] * op2[j];
    }

    public static float iprod(Vector operand1, Vector operand2){
        // inner product of two vector
        float[] op1 = operand1.data;
        float[] op2 = operand2.data;
        float dst = 0.0f;
        for(int i=0; i<op1.length; i++)
            dst += op1[i] * op2[i];
        return dst;
    }

    public static void relu(Vector operand, Vector destination){
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++){
            if(op[i] < 0.0f)
                dst[i] = 0.0f;
            else
                dst[i] = op[i];
        }
    }

    public static void drelu(Vector operand, Vector destination){
        // The derivitive of ReLU function
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++){
            if(op[i] == 0.0f)
                dst[i] = 0.0f;
            else
                dst[i] = 1.0f;
        }
    }

    public static void sigmoid(Vector operand, Vector destination){
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++)
            dst[i] = 1.0f / (1.0f + (float)Math.exp(-1 * op[i]));
    }

    public static void dsigmoid(Vector operand, Vector destination){
        // The derivitive of sigmoid function
        float[] op = operand.data;
        float[] dst = destination.data;
        for(int i=0; i<op.length; i++)
            dst[i] = op[i] * (1.0f - op[i]);
    }

    public static void oneHot(Vector operand, int which){
        float[] op = operand.data;
        for(int i=0; i<op.length; i++)
            op[i] = 0.0f;
        op[which] = 1.0f;
    }

    public static int maxOverZero(Vector operand){
        float[] op = operand.data;
        float max = 0.0f;
        int which = 0;
        for(int i=0; i<op.length; i++){
            if(op[i] > max){
                max = op[i];
                which = i;
            }
        }
        return which;
    }

    public static void rand(Vector operand, float limit){
        // randomize vector in range [-limit, limit]
        float[] op = operand.data;
        Random rnd = new Random();
        float divide = 1.0f / limit / 2.0f;
        for(int i=0; i<op.length; i++)
            op[i] = rnd.nextFloat() / divide - limit;
    }

    public static void range(Vector operand, float start, float delta){
        // initialize vector to be an arithmetic sequence
        float[] op = operand.data;
        for(int i=0; i<op.length; i++)
            op[i] = start + i * delta;
    }

    public static void range(Vector operand){
        range(operand, 0.0f, 1.0f);
    }

    public static void main(String[] args){
        Vector v1 = new Vector(3);
        VectorMath.rand(v1, 0.1f);
        System.out.println(v1);

        v1 = new Vector(3);
        VectorMath.range(v1);
        v1.data[0] = 1.0f;

        Vector v2 = new Vector(3);
        VectorMath.range(v2);

        VectorMath.add(v1, v2, v1);
        VectorMath.add(v1, v1, v1);
        VectorMath.minus(v2, v1, v2);
        System.out.println(v1);
        System.out.println(v2);

        VectorMath.mult(v2, 3.0f, v2);
        v2.data[0] = 10.0f;
        System.out.println(v2);

        VectorMath.div(v2, 2.0f, v1);
        System.out.println(v1);

        System.out.println(VectorMath.iprod(v1, v2));

        System.out.println("");
        Matrix m = new Matrix(3, 3);
        VectorMath.cprod(v1, v2, m);
        System.out.println(m);
    }
}

class FileParser {
    private static void parseHeader(String filename, HashMap<String, Integer> featureToVal, HashMap<String, Float> labelToVal){
            labelToVal.put("_", new Float(0.0));
            labelToVal.put("e", new Float(1.0));
            labelToVal.put("h", new Float(2.0));
            featureToVal.put("Z", new Integer(0));
            try{
                BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
                String content;
                while((content = reader.readLine()) != null){
                    content = content.trim();
                    if(content.length() == 0 || content.startsWith("#"))
                        continue;
                    if(content.equals("<>") || content.equals("end") || content.equals("<end>"))
                        continue;
                    String name = content.split(" ")[0].trim();
                    if(!featureToVal.containsKey(name))
                        featureToVal.put(name, new Integer(featureToVal.size()));
                }
                reader.close();
            }catch(IOException e){
                e.printStackTrace();
            }
    }

    public static void loadFromFile(String filename, Matrix featureArr, Vector labelArr,
                                    HashMap<String, Integer> featureToVal, HashMap<String, Float> labelToVal, int windowSize){
        if(featureToVal.size() == 0 && labelToVal.size() == 0)
            parseHeader(filename, featureToVal, labelToVal);
        ArrayList<float[]> feature = new ArrayList<float[]>();
        ArrayList<Float> label = new ArrayList<Float>();
        try{
            BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
            String content = null;
            int padding = (windowSize - 1) / 2;
            while(true){
                if(content == null || !content.equals("<>")){
                    content = reader.readLine();
                    if(content == null)
                        break;
                }
                content = content.trim();
                if(content.length() == 0 || content.startsWith("#"))
                    continue;
                if(content.equals("<>")){
                    // setup the beginning with padding
                    int featureCount = featureToVal.size();
                    // input vector contains a 1.0 for learning bias
                    float[] array = new float[windowSize * featureCount + 1];
                    for(int i=0; i<array.length; i++)
                        array[i] = 0.0f;
                    array[windowSize * featureCount] = 1.0f;
                    for(int i=0; i<padding; i++)
                        array[featureCount * i + featureToVal.get("Z")] = 1.0f;
                    for(int i=padding; i<windowSize; i++){
                        // here assume the length of protein sequence is at least padding + 1
                        content = reader.readLine().trim();
                        String[] lists = content.split(" ");
                        array[featureCount * i + featureToVal.get(lists[0])] = 1.0f;
                        label.add(labelToVal.get(lists[1]));
                    }
                    float[] f = array.clone();
                    feature.add(f);
                    // build features for each amino acid
                    while((content = reader.readLine()) != null){
                        content = content.trim();
                        if(content.equals("end") || content.equals("<end>") || content.equals("<>"))
                            break;
                        for(int i=featureCount; i<windowSize * featureCount; i++)
                            array[i - featureCount] = array[i];
                        for(int i=(windowSize-1) * featureCount; i<windowSize * featureCount; i++)
                            array[i] = 0.0f;
                        String[] lists = content.split(" ");
                        array[(windowSize-1) * featureCount + featureToVal.get(lists[0])] = 1.0f;
                        label.add(labelToVal.get(lists[1]));
                        f = array.clone();
                        feature.add(f);
                    }
                    // setup the ending with padding
                    for(int i=0; i<padding; i++){
                        for(int j=featureCount; j<windowSize * featureCount; j++)
                            array[j - featureCount] = array[j];
                        for(int j=(windowSize-1) * featureCount; j<windowSize * featureCount; j++)
                            array[j] = 0.0f;
                        array[(windowSize-1) * featureCount + featureToVal.get("Z")] = 1.0f;
                        f = array.clone();
                        feature.add(f);
                    }
                }
            }
            reader.close();
        }catch(IOException e){
            e.printStackTrace();
        }
        float[][] matrix = new float[feature.size()][];
        for(int i=0; i<feature.size(); i++)
            matrix[i] = feature.get(i);
        float[] vector = new float[label.size()];
        for(int i=0; i<label.size(); i++)
            vector[i] = label.get(i);
        featureArr.set(matrix);
        labelArr.set(vector);
    }

    public static void splitTrainTuneTest(String filename){
        try{
            BufferedReader reader = new BufferedReader(new FileReader(new File(filename)));
            BufferedWriter train = new BufferedWriter(new FileWriter(new File(filename + ".train")));
            BufferedWriter tune = new BufferedWriter(new FileWriter(new File(filename + ".tune")));
            BufferedWriter test = new BufferedWriter(new FileWriter(new File(filename + ".test")));
            BufferedWriter writer;
            String content = null;
            int counter = 0;
            while(true){
                if(content == null || !content.equals("<>")){
                    content = reader.readLine();
                    if(content == null)
                        break;
                }
                content = content.trim();
                if(content.length() == 0 || content.startsWith("#"))
                    continue;
                if(content.equals("<>")){
                    counter++;
                    if(counter % 5 == 0)
                        writer = tune;
                    else if(counter > 1 && counter % 5 == 1)
                        writer = test;
                    else
                        writer = train;
                    writer.write("<>\n");
                    while((content = reader.readLine()) != null){
                        content = content.trim();
                        if(content.equals("<>"))
                            break;
                        writer.write(content);
                        writer.write("\n");
                        if(content.equals("end") || content.equals("<end>"))
                            break;
                    }
                }
            }
            test.close();
            tune.close();
            train.close();
            reader.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}

class Layer {
    // weight includes the learning bias w0
    public Matrix weight;
    // output of this layer
    public Vector output;
    // diff on the output
    public Vector diff;
    // delta to weight
    public Matrix delta;
    // delta momentum
    public Matrix momentum;

    // some temp vector to store middle result
    public Vector temp;
    public Matrix tempm;

    public Layer(int dx, int dy, float rand_limit){
        weight = new Matrix(dx, dy);
        output = new Vector(dx);
        diff = new Vector(dx);
        delta = new Matrix(dx, dy);
        momentum = new Matrix(dx, dy);
        MatrixMath.zero(momentum);
        temp = new Vector(dx);
        tempm = new Matrix(dx, dy);
        MatrixMath.rand(weight, rand_limit);
    }
}

public class Lab2 {
    // a pointer which will point to one of the following three feature matrixs
    private Matrix feature;
    private Matrix feature_train;
    private Matrix feature_tune;
    private Matrix feature_test;

    // a pointer which will point to one of the following three label vectors
    private Vector label;
    private Vector label_train;
    private Vector label_tune;
    private Vector label_test;

    private HashMap<String, Integer> feature_to_val;
    private HashMap<String, Float> label_to_val;
    private HashMap<Float, String> val_to_label;

    private float weight_decay;
    private float rand_limit;
    private float learning_rate;
    private float momentum_factor;

    private int window_size;
    private int early_stopping_factor;

    private Layer hidden;
    private Layer output;

    private Matrix best_hidden_weight;
    private Matrix best_output_weight;

    public Lab2(){
        feature_train = new Matrix();
        feature_tune = new Matrix();
        feature_test = new Matrix();

        label_train = new Vector();
        label_tune = new Vector();
        label_test = new Vector();

        feature_to_val = new HashMap<String, Integer>();
        label_to_val = new HashMap<String, Float>();
        val_to_label = new HashMap<Float, String>();

        weight_decay = 0.001f;
        rand_limit = 0.03f;
        learning_rate = 0.0005f;
        momentum_factor = 0.9f;
        early_stopping_factor = 5;
        window_size = 17;
    }

    public void init(String filename, int hiddenDim){
        FileParser.splitTrainTuneTest(filename);
        FileParser.loadFromFile(filename + ".train", feature_train, label_train, feature_to_val, label_to_val, window_size);
        FileParser.loadFromFile(filename + ".tune", feature_tune, label_tune, feature_to_val, label_to_val, window_size);
        FileParser.loadFromFile(filename + ".test", feature_test, label_test, feature_to_val, label_to_val, window_size);
        // input dimension +1 for learning bias
        // numHidden + 1 for learning bias on hidden level
        int inputDim = window_size * feature_to_val.size() + 1;
        hiddenDim = hiddenDim + 1;
        int outputDim = label_to_val.size();
        hidden = new Layer(hiddenDim, inputDim, rand_limit);
        output = new Layer(outputDim, hiddenDim, rand_limit);
        best_hidden_weight = new Matrix(hiddenDim, inputDim);
        best_output_weight = new Matrix(outputDim, hiddenDim);
    }

    public void forward(int i){
        Vector input = feature.get(i);
        // forward in hidden layer
        MatrixMath.lprod(hidden.weight, input, hidden.output);
        // Hidden unit use ReLU
        VectorMath.relu(hidden.output, hidden.output);
        // Hidden unit use sigmoid function
        //VectorMath.sigmoid(hidden.output, hidden.output);

        // mark the last output of hidden to be 1.0f, since this is bias
        int which = hidden.output.data.length - 1;
        hidden.output.data[which] = 1.0f;
        // forward in output layer
        MatrixMath.lprod(output.weight, hidden.output, output.output);
        VectorMath.sigmoid(output.output, output.output);
    }

    public void backward(int i){
        // construct std label
        int correct = (int)(label.get(i));
        int labelSize = label_to_val.size();
        int hiddenSize = hidden.weight.data.length;
        Vector input = feature.get(i);
        Vector y = new Vector(labelSize);
        VectorMath.oneHot(y, correct);

        // calculate delta value on output
        VectorMath.minus(y, output.output, output.diff);
        MatrixMath.rprod(output.diff, output.weight, hidden.diff);
        // Hidden unit use ReLu function
        VectorMath.drelu(hidden.output, hidden.temp);
        // Hidden unit use sigmoid function
        //VectorMath.dsigmoid(hidden.output, hidden.temp);
        VectorMath.vmult(hidden.temp, hidden.diff, hidden.diff);

        // calculate delta value on weight
        VectorMath.cprod(output.diff, hidden.output, output.delta);
        MatrixMath.mult(output.delta, learning_rate, output.delta);
            MatrixMath.mult(output.momentum, momentum_factor, output.tempm);
            MatrixMath.add(output.delta, output.tempm, output.delta);
                MatrixMath.mult(output.weight, learning_rate * weight_decay, output.tempm);
        MatrixMath.add(output.weight, output.delta, output.weight);
            MatrixMath.copy(output.delta, output.momentum);
                MatrixMath.minus(output.weight, output.tempm, output.weight);

        
        VectorMath.cprod(hidden.diff, input, hidden.delta);
        MatrixMath.mult(hidden.delta, learning_rate, hidden.delta);
            MatrixMath.mult(hidden.momentum, momentum_factor, hidden.tempm);
            MatrixMath.add(hidden.delta, hidden.tempm, hidden.delta);
                MatrixMath.mult(hidden.weight, learning_rate * weight_decay, hidden.tempm);
        MatrixMath.add(hidden.weight, hidden.delta, hidden.weight);
            MatrixMath.copy(output.delta, output.momentum);
                MatrixMath.minus(hidden.weight, hidden.tempm, hidden.weight);
    }

    public void trainEpoch(){
        int numFeature = feature.data.length;
        for(int i=0; i<numFeature; i++){
            forward(i);
            backward(i);
        }
    }

    public void train(){
        float best_accuracy = 0.0f;
        int count = 0;
        for(int i=0; i<50; i++){
            feature = feature_train;
            label = label_train;
            trainEpoch();
            feature = feature_tune;
            label = label_tune;
            float accuracy = testEpoch(false);
            System.out.print("Tune set accuracy for ");
            System.out.print(i);
            System.out.print(" epoch: ");
            System.out.println(accuracy);
            // stop training when accuracy on tune dataset does not increase
            if(accuracy > best_accuracy){
                best_accuracy = accuracy;
                MatrixMath.copy(hidden.weight, best_hidden_weight);
                MatrixMath.copy(output.weight, best_output_weight);
                count = 0;
            }
            else{
                count += 1;
                if(count == early_stopping_factor)
                    break;
            }
        }
        hidden.weight = best_hidden_weight;
        output.weight = best_output_weight;
    }

    public float testEpoch(boolean isPrint){
        int numFeature = feature.data.length;
        int counter = 0;
        for(int i=0; i<numFeature; i++){
            forward(i);
            int answer = VectorMath.maxOverZero(output.output);
            int correct = (int)(label.get(i));
            if(answer == correct)
                counter++;
            if(isPrint)
                System.out.println(val_to_label.get(new Float(answer)));
        }
        return (float)(counter) / (float)(numFeature);
    }

    public void test(){
        feature = feature_test;
        label = label_test;
        for(HashMap.Entry<String, Float> entry : label_to_val.entrySet()){
            val_to_label.put(entry.getValue(), entry.getKey());
        }
        float accuracy = testEpoch(true);
        System.out.print("Test set accuracy: ");
        System.out.println(accuracy);
    }

    public static void main(String[] args) {
        Lab2 nn = new Lab2();
        nn.init(args[0], 50);
        nn.train();
        nn.test();
        //nn.iotest(args[0]);
    }
}
