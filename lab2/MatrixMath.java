package cs838;

import java.util.*;

class Matrix {
    public float[][] data;

    public Vector get(int i){
        return new Vector(data[i]);
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i=0; i<data.length; i++){
            sb.append("[");
            for(int j=0; j<data[0].length-1; j++){
                sb.append(data[i][j]);
                sb.append(", ");
            }
            sb.append(data[i][data[0].length-1]);
            if(i == data.length-1)
                sb.append("]]");
            else
                sb.append("],\n");
        }
        return sb.toString();
    }

    public void set(float[][] source){
        data = source;
    }

    public void empty_like(Matrix operand){
        float[][] op = operand.data;
        data = new float[op.length][op[0].length];
    }

    // Different construction functions
    public Matrix(float[][] source){
        data = source;
    }

    public Matrix(int dx, int dy){
        data = new float[dx][dy];
    }

    public Matrix(){
        data = null;
    }
}

public class MatrixMath{
    public static Random rnd = new Random();

    public static void add(Matrix operand1, Matrix operand2, Matrix destination){
        float[][] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[][] dst = destination.data;
        for(int i=0; i<op1.length; i++)
            for(int j=0; j<op1[0].length; j++)
                dst[i][j] = op1[i][j] + op2[i][j];
    }

    public static void minus(Matrix operand1, Matrix operand2, Matrix destination){
        float[][] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[][] dst = destination.data;
        for(int i=0; i<op1.length; i++)
            for(int j=0; j<op1[0].length; j++)
                dst[i][j] = op1[i][j] - op2[i][j];
    }

    public static void mult(Matrix operand, float val, Matrix destination){
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                dst[i][j] = op[i][j] * val;
    }

    public static void div(Matrix operand, float val, Matrix destination){
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                dst[i][j] = op[i][j] / val;
    }

    public static void lprod(Matrix operand1, Vector operand2, Vector destination){
        // Inner product each line of a matrix with a vector
        // Matrix(a*b) * Vector(b) = Vector(a)
        float[][] op1 = operand1.data;
        float[] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<op1.length; i++){
            float sum = 0.0f;
            for(int j=0; j<op2.length; j++)
                sum += op1[i][j] * op2[j];
            dst[i] = sum;
        }
    }

    public static void rprod(Vector operand1, Matrix operand2, Vector destination){
        // Inner product a vector with 
        // Vector(a) * Matrix(a*b) = Vector(b)
        float[] op1 = operand1.data;
        float[][] op2 = operand2.data;
        float[] dst = destination.data;
        for(int i=0; i<op2[0].length; i++){
            float sum = 0.0f;
            for(int j=0; j<op1.length; j++)
                sum += op1[j] * op2[j][i];
            dst[i] = sum;
        }
    }

    public static void rand(Matrix operand, float limit){
        // randomize matrix in range [-limit, limit]
        float[][] op = operand.data;
        float divide = 1.0f / limit / 2.0f;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                op[i][j] = rnd.nextFloat() / divide - limit;
    }

    public static void range(Matrix operand, float start, float delta){
        // initialize matrix to be a arithmetic sequence
        float[][] op = operand.data;
        int counter = 0;
        for(int i=0; i<op.length; i++){
            for(int j=0; j<op[0].length; j++){
                op[i][j] = start + counter * delta;
                counter++;
            }
        }
    }

    public static void range(Matrix operand){
        range(operand, 0.0f, 1.0f);
    }

    public static void zero(Matrix operand){
        float[][] op = operand.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                op[i][j] = 0.0f;
    }

    public static void copy(Matrix operand, Matrix destination){
        float[][] op = operand.data;
        float[][] dst = destination.data;
        for(int i=0; i<op.length; i++)
            for(int j=0; j<op[0].length; j++)
                dst[i][j] = op[i][j];
    }

    public static void main(String[] args){
        Matrix m1 = new Matrix(3, 3);
        MatrixMath.rand(m1, 30.0f);
        System.out.println(m1);

        m1 = new Matrix(3, 3);
        MatrixMath.range(m1);
        m1.data[0][0] = 1.0f;
        System.out.println(m1);
        System.out.println("");

        Vector v1 = new Vector(3);
        VectorMath.range(v1);
        v1.data[0] = 1.0f;
        System.out.println(v1);
        System.out.println("");

        Vector res = new Vector();
        res.empty_like(v1);
        MatrixMath.lprod(m1, v1, res);
        System.out.println(res);        
        System.out.println("");

        System.out.println(m1);
        Vector v = m1.get(0);
        v.data[0] = 100;
        System.out.println(m1);
    }
}