package cs838;

import java.util.*;

class Layer {
    // weight includes the learning bias w0
    public Matrix weight;
    // output of this layer
    public Vector output;
    // diff on the output
    public Vector diff;
    // delta to weight
    public Matrix delta;
    // delta momentum
    public Matrix momentum;

    // some temp vector to store middle result
    public Vector temp;
    public Matrix tempm;

    public Layer(int dx, int dy, float rand_limit){
        weight = new Matrix(dx, dy);
        output = new Vector(dx);
        diff = new Vector(dx);
        delta = new Matrix(dx, dy);
        momentum = new Matrix(dx, dy);
        MatrixMath.zero(momentum);
        temp = new Vector(dx);
        tempm = new Matrix(dx, dy);
        MatrixMath.rand(weight, rand_limit);
    }
}

public class Lab2 {
    // a pointer which will point to one of the following three feature matrixs
    private Matrix feature;
    private Matrix feature_train;
    private Matrix feature_tune;
    private Matrix feature_test;

    // a pointer which will point to one of the following three label vectors
    private Vector label;
    private Vector label_train;
    private Vector label_tune;
    private Vector label_test;

    private HashMap<String, Integer> feature_to_val;
    private HashMap<String, Float> label_to_val;
    private HashMap<Float, String> val_to_label;

    private float weight_decay;
    private float rand_limit;
    private float learning_rate;
    private float momentum_factor;

    private int window_size;
    private int early_stopping_factor;

    private Layer hidden;
    private Layer output;

    private Matrix best_hidden_weight;
    private Matrix best_output_weight;

    public Lab2(){
        feature_train = new Matrix();
        feature_tune = new Matrix();
        feature_test = new Matrix();

        label_train = new Vector();
        label_tune = new Vector();
        label_test = new Vector();

        feature_to_val = new HashMap<String, Integer>();
        label_to_val = new HashMap<String, Float>();
        val_to_label = new HashMap<Float, String>();

        weight_decay = 0.001f;
        rand_limit = 0.03f;
        learning_rate = 0.0005f;
        momentum_factor = 0.9f;
        early_stopping_factor = 5;
        window_size = 17;
    }

    public void init(String filename, int hiddenDim){
        FileParser.splitTrainTuneTest(filename);
        FileParser.loadFromFile(filename + ".train", feature_train, label_train, feature_to_val, label_to_val, window_size);
        FileParser.loadFromFile(filename + ".tune", feature_tune, label_tune, feature_to_val, label_to_val, window_size);
        FileParser.loadFromFile(filename + ".test", feature_test, label_test, feature_to_val, label_to_val, window_size);
        // input dimension +1 for learning bias
        // numHidden + 1 for learning bias on hidden level
        int inputDim = window_size * feature_to_val.size() + 1;
        hiddenDim = hiddenDim + 1;
        int outputDim = label_to_val.size();
        hidden = new Layer(hiddenDim, inputDim, rand_limit);
        output = new Layer(outputDim, hiddenDim, rand_limit);
        best_hidden_weight = new Matrix(hiddenDim, inputDim);
        best_output_weight = new Matrix(outputDim, hiddenDim);
    }

    public void forward(int i){
        Vector input = feature.get(i);
        // forward in hidden layer
        MatrixMath.lprod(hidden.weight, input, hidden.output);
        // Hidden unit use ReLU
        VectorMath.relu(hidden.output, hidden.output);
        // Hidden unit use sigmoid function
        //VectorMath.sigmoid(hidden.output, hidden.output);

        // mark the last output of hidden to be 1.0f, since this is bias
        int which = hidden.output.data.length - 1;
        hidden.output.data[which] = 1.0f;
        // forward in output layer
        MatrixMath.lprod(output.weight, hidden.output, output.output);
        VectorMath.sigmoid(output.output, output.output);
    }

    public void backward(int i){
        // construct std label
        int correct = (int)(label.get(i));
        int labelSize = label_to_val.size();
        int hiddenSize = hidden.weight.data.length;
        Vector input = feature.get(i);
        Vector y = new Vector(labelSize);
        VectorMath.oneHot(y, correct);

        // calculate delta value on output
        VectorMath.minus(y, output.output, output.diff);
        MatrixMath.rprod(output.diff, output.weight, hidden.diff);
        // Hidden unit use ReLu function
        VectorMath.drelu(hidden.output, hidden.temp);
        // Hidden unit use sigmoid function
        //VectorMath.dsigmoid(hidden.output, hidden.temp);
        VectorMath.vmult(hidden.temp, hidden.diff, hidden.diff);

        // calculate delta value on weight
        VectorMath.cprod(output.diff, hidden.output, output.delta);
        MatrixMath.mult(output.delta, learning_rate, output.delta);
            MatrixMath.mult(output.momentum, momentum_factor, output.tempm);
            MatrixMath.add(output.delta, output.tempm, output.delta);
                MatrixMath.mult(output.weight, learning_rate * weight_decay, output.tempm);
        MatrixMath.add(output.weight, output.delta, output.weight);
            MatrixMath.copy(output.delta, output.momentum);
                MatrixMath.minus(output.weight, output.tempm, output.weight);

        
        VectorMath.cprod(hidden.diff, input, hidden.delta);
        MatrixMath.mult(hidden.delta, learning_rate, hidden.delta);
            MatrixMath.mult(hidden.momentum, momentum_factor, hidden.tempm);
            MatrixMath.add(hidden.delta, hidden.tempm, hidden.delta);
                MatrixMath.mult(hidden.weight, learning_rate * weight_decay, hidden.tempm);
        MatrixMath.add(hidden.weight, hidden.delta, hidden.weight);
            MatrixMath.copy(output.delta, output.momentum);
                MatrixMath.minus(hidden.weight, hidden.tempm, hidden.weight);
    }

    public void trainEpoch(){
        int numFeature = feature.data.length;
        for(int i=0; i<numFeature; i++){
            forward(i);
            backward(i);
        }
    }

    public void train(){
        float best_accuracy = 0.0f;
        int count = 0;
        for(int i=0; i<50; i++){
            feature = feature_train;
            label = label_train;
            trainEpoch();
            feature = feature_tune;
            label = label_tune;
            float accuracy = testEpoch(false);
            System.out.print("Tune set accuracy for ");
            System.out.print(i);
            System.out.print(" epoch: ");
            System.out.println(accuracy);
            // stop training when accuracy on tune dataset does not increase
            if(accuracy > best_accuracy){
                best_accuracy = accuracy;
                MatrixMath.copy(hidden.weight, best_hidden_weight);
                MatrixMath.copy(output.weight, best_output_weight);
                count = 0;
            }
            else{
                count += 1;
                if(count == early_stopping_factor)
                    break;
            }
        }
        hidden.weight = best_hidden_weight;
        output.weight = best_output_weight;
    }

    public float testEpoch(boolean isPrint){
        int numFeature = feature.data.length;
        int counter = 0;
        for(int i=0; i<numFeature; i++){
            forward(i);
            int answer = VectorMath.maxOverZero(output.output);
            int correct = (int)(label.get(i));
            if(answer == correct)
                counter++;
            if(isPrint)
                System.out.println(val_to_label.get(new Float(answer)));
        }
        return (float)(counter) / (float)(numFeature);
    }

    public void test(){
        feature = feature_test;
        label = label_test;
        for(HashMap.Entry<String, Float> entry : label_to_val.entrySet()){
            val_to_label.put(entry.getValue(), entry.getKey());
        }
        float accuracy = testEpoch(true);
        System.out.print("Test set accuracy: ");
        System.out.println(accuracy);
    }

    public static void main(String[] args) {
        Lab2 nn = new Lab2();
        nn.init(args[0], 50);
        nn.train();
        nn.test();
        //nn.iotest(args[0]);
    }
}
